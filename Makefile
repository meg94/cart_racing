CC := gcc

BIN := prog
SOURCES := $(shell find src -iname '*.cpp')
OBJS := $(SOURCES:.cpp=.o)
DEPS = $(SOURCES:%.cpp=%.d)
CFLAGS := -Isrc -IBullet/src -Wall -isystemsrc/single_file_libs -DLINUX_BUILD -g
LIBS := -LBullet -lstdc++ -lGL -lGLU -lGLEW -lglfw -lm -lBulletDynamics -lBulletCollision -lLinearMath

all: $(BIN)

$(BIN): $(OBJS)
	$(CC) -o $@ $(OBJS) $(LIBS) 

$(OBJS): %.o: %.cpp
	$(CC) $(CFLAGS) -c -MMD -o $@ $<

-include $(DEPS)

clean:
	rm -f $(BIN) $(OBJS) $(DEPS)
