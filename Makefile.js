CC := emcc

BIN := prog.html
SOURCES := $(shell find src -iname '*.cpp')
OBJS := $(SOURCES:.cpp=.o)
DEPS = $(SOURCES:%.cpp=%.d)
CFLAGS := -O2 -Isrc -IBullet/src -Wall --preload-file assets/ -s ALLOW_MEMORY_GROWTH=1 -s USE_WEBGL2=1 -s FULL_ES3=1 -s USE_GLFW=3 -isystemsrc/single_file_libs -DJS_BUILD -g
LIBS := -lstdc++ -lGL -lGLU -lGLEW -lglfw -lm

all: $(BIN)

$(BIN): $(OBJS)
	$(CC) -o $@ Bullet/libBulletDynamics.bc Bullet/libBulletCollision.bc Bullet/libLinearMath.bc $(OBJS) $(LIBS) $(CFLAGS)

$(OBJS): %.o: %.cpp
	$(CC) $(CFLAGS) -c -MMD -o $@ $<

-include $(DEPS)

clean:
	rm -f $(BIN) $(OBJS) $(DEPS)
