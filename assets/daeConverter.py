from pyassimp import *

def write_line(file, line):
    file.write(line + "\n")

def recur_node(node, output_file):
    write_line(output_file, node.name + " " + str(len(node.children)) + " " + str(len(node.meshes))) 
    write_line(output_file, str(node.transformation))
    for mesh in node.meshes:
        write_line(output_file, str(mesh.materialindex))
        assert(len(mesh.texturecoords) == 1)

        write_line(output_file, str(len(mesh.vertices)))
        for vertex in mesh.vertices:
            write_line(output_file, str(vertex))

        write_line(output_file, str(len(mesh.normals)))
        for normal in mesh.normals:
            write_line(output_file, str(normal))

        write_line(output_file, str(len(mesh.texturecoords[0])))
        for texture_coord in mesh.texturecoords[0]:
            write_line(output_file, str(texture_coord))

        write_line(output_file, str(len(mesh.faces)))
        for face in mesh.faces:
            write_line(output_file, str(face))

    for child in node.children:
        recur_node(child,  output_file)

def load_dae(input_filename, output_filename):
    scene = load(input_filename)
    output_file = open(output_filename, 'w')

    write_line(output_file, str(len(scene.materials)))
    for material in scene.materials:
        kd = material.properties['diffuse']
        if (len(kd) == 3):
            kd = kd + [0.0]
        assert(len(kd) == 4)
        write_line(output_file, str(kd))
    recur_node(scene.rootnode, output_file)

    output_file.close()
    release(scene)

f = open('model-list.txt', 'r')
lines = f.readlines()
f.close()

for line in lines:
    name = line[:-1]
    load_dae('dae-models/' + name + '.dae', 'models/' + name + '.model_data')
