#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <map>
#include <string>

#include "drawing.h"
#include "entity.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

GLuint load_texture(const char *filename, int filter) {
    int x, y, n;
    int force_channels = 4;

    stbi_set_flip_vertically_on_load(0);
    unsigned char *image_data = stbi_load(filename, &x, &y, &n, force_channels);

    if (!image_data) {
        printf("Could not load texture file %s\n", filename);
        return 0;
    }

    GLuint location;
    glGenTextures(1, &location);
    glBindTexture(GL_TEXTURE_2D, location);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(image_data);

    return location;
}

static void shader_check_for_shader_compile_errors(GLuint program, const char *name) {
    int params = -1;
    glGetShaderiv(program, GL_COMPILE_STATUS, &params);
    if (params != GL_TRUE) {
        char log[2048];
        int length;
        glGetShaderInfoLog(program, 2048, &length, log);

        printf("Could not compile shader: %s\n", name);
        printf("Shader log:\n%s\n", log);
    }
}

static GLuint load_shader(GLuint type, const char *shader_code) {
    GLuint shader = glCreateShader(type);

    const char *shader_version_string = "#version 300 es\n";

    int shader_code_with_version_len = strlen(shader_version_string) + strlen(shader_code) + 1;
    char shader_code_with_version[shader_code_with_version_len];

    strcpy(shader_code_with_version, shader_version_string);
    strcpy(shader_code_with_version + strlen(shader_version_string), shader_code);

    const char *stupid_const = (const char *)shader_code_with_version;
    glShaderSource(shader, 1, &stupid_const, NULL);
    glCompileShader(shader);

    return shader;
}

static GLint shader_create_program(const char *vertex_shader_code, const char *fragment_shader_code) {
    GLint vertex_shader = load_shader(GL_VERTEX_SHADER, vertex_shader_code);
    shader_check_for_shader_compile_errors(vertex_shader, "Vertex Shader");

    GLint fragment_shader = load_shader(GL_FRAGMENT_SHADER, fragment_shader_code);
    shader_check_for_shader_compile_errors(fragment_shader, "Fragment Shader");

    GLint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    GLint params = -1;
    glGetProgramiv(program, GL_LINK_STATUS, &params);
    if (params != GL_TRUE) {
        char log[2048];
        int length;
        glGetProgramInfoLog(program, 2048, &length, log);

        printf("Could not link shader program.\n");
        printf("Program log:\n%s\n", log);
    }

    return program;
}

static int load_dae_node(FILE *f, struct geometry_array *geometry_array) {
    char node_name[256];
    int num_meshes, num_child_nodes;
    assert(fscanf(f, "%s %d %d\n", node_name, &num_child_nodes, &num_meshes) == 3);

    mat4 t;
    assert(fscanf(f, 
                "[[%f, %f, %f, %f], [%f, %f, %f, %f], [%f, %f, %f, %f], [%f, %f, %f, %f]]\n",
                &t.m[0], &t.m[1], &t.m[2], &t.m[3],
                &t.m[4], &t.m[5], &t.m[6], &t.m[7],
                &t.m[8], &t.m[9], &t.m[10], &t.m[11],
                &t.m[12], &t.m[13], &t.m[14], &t.m[15]) == 16); 

    for (int i = 0; i < num_meshes; i++) {
        int geometry_idx = geometry_array->num_geometries++;
        struct geometry *geometry = &geometry_array->geometries[geometry_idx]; 
        geometry->transformation = t;

        int material_index; 
        assert(fscanf(f, "%d\n", &material_index) == 1);
        geometry->material_index = material_index;

        int num_vertices;
        assert(fscanf(f, "%d\n", &num_vertices) == 1);
        geometry->num_vertices = num_vertices;
        for (int j = 0; j < num_vertices; j++) {
            vec3 v;
            assert(fscanf(f, "[%f, %f, %f]\n", &v.x, &v.y, &v.z) == 3);
            geometry->vertices[j] = v;
        }

        for (int j = 0; j < num_vertices; j++) {
            geometry->transformed_vertices[j] = vec3_apply_mat4(geometry->vertices[j], 1.0f,
                    geometry->transformation); 
        }

        int num_normals;
        assert(fscanf(f, "%d\n", &num_normals) == 1);
        for (int j = 0; j < num_normals; j++) {
            vec3 n;
            assert(fscanf(f, "[%f, %f, %f]\n", &n.x, &n.y, &n.z) == 3);
            geometry->normals[j] = n;
        } 

        int num_texture_coords;
        assert(fscanf(f, "%d\n", &num_texture_coords) == 1);
        for (int j = 0; j < num_texture_coords; j++) {
            vec3 tc;
            assert(fscanf(f, "[%f, %f, %f]\n", &tc.x, &tc.y, &tc.z) == 3);
            geometry->texture_coords[j] = V2(tc.x, tc.y);
        } 

        int num_faces;
        assert(fscanf(f, "%d\n", &num_faces) == 1);
        geometry->num_faces = num_faces;
        for (int j = 0; j < num_faces; j++) {
            int f0, f1, f2;
            assert(fscanf(f, "[%dL, %dL, %dL]\n", &f0, &f1, &f2) == 3);
            geometry->faces[j] = create_geometry_face(f0, f1, f2);
        }

        assert(num_vertices == num_normals);
        assert(num_vertices == num_texture_coords);

        glGenBuffers(1, &geometry->vertices_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, geometry->vertices_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * num_vertices,
                geometry->vertices, GL_STATIC_DRAW);

        glGenBuffers(1, &geometry->normals_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, geometry->normals_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * num_normals,
                geometry->normals, GL_STATIC_DRAW);

        glGenBuffers(1, &geometry->texture_coords_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, geometry->texture_coords_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * num_texture_coords,
                geometry->texture_coords, GL_STATIC_DRAW);

        glGenBuffers(1, &geometry->faces_vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->faces_vbo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(struct geometry_face) * num_faces, 
                geometry->faces, GL_STATIC_DRAW);
    }

    for (int i = 0; i < num_child_nodes; i++) {
        load_dae_node(f, geometry_array);
    }

    return 0;
}

int load_dae(const char *filename, struct geometry_array *geometry_array, struct material *material) {
    FILE *f = fopen(filename, "r");
    assert(f);

    material->type = MATERIAL_TYPE_OBJ;

    int num_materials;
    assert(fscanf(f, "%d\n", &num_materials) == 1);
    for (int i = 0; i < num_materials; i++) {
        vec4 kd;
        assert(fscanf(f, "[%f, %f, %f, %f]\n", &kd.x, &kd.y, &kd.z, &kd.w) == 4); 
        material->obj_material.colors[i] = V3(kd.x, kd.y, kd.z);
    }

    geometry_array->num_geometries = 0;
    load_dae_node(f, geometry_array);

    fclose(f);

    return 0;
}

void init_renderer_state(struct renderer_state *state) {
    state->num_drawables = 0;
    state->num_ui_drawables = 0;

    state->proj_mat = mat4_identity();
    state->view_mat = mat4_identity();
    state->ui_proj_mat = mat4_identity();
    state->ui_view_mat = mat4_identity();
    state->car_proj_mat = mat4_identity();
    state->car_view_mat = mat4_identity();

    state->floor_tex = load_texture("assets/floor.jpg", GL_LINEAR);

    {
        int size = 2048;

        state->shadow_position = V3(0.0f, 0.0f, 0.0f);
        state->shadow_direction = V3(1.0f, -10.0f, -1.0f);
        state->shadow_up = V3(0.0f, 1.0f, 0.0f);
        state->shadow_x = 100.0f;
        state->shadow_y = 100.0f;
        state->shadow_z = 10.0f;

        GLuint fb;
        glGenFramebuffers(1, &fb);
        glBindFramebuffer(GL_FRAMEBUFFER, fb);

        GLuint tex;
        glGenTextures(1, &tex);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, size, size, 
                0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex, 0);
        GLenum draw_bufs[] = { GL_NONE };
        glDrawBuffers(1, draw_bufs);

        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        state->shadow_size = size;
        state->shadow_fb = fb;
        state->shadow_tex = tex;
    }

    {
        const char *vertex_shader =
            "in vec3 v;"

            "uniform mat4 mvp_mat;"

            "void main() {"
            "   gl_Position = mvp_mat * vec4(v, 1.0);"
            "}";

        const char *fragment_shader =
            "precision highp float;"

            "void main() {"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint v_loc = glGetAttribLocation(program, "v");

        state->shadow_shader.program = (GLuint)program;
        state->shadow_shader.mvp_mat_loc = (GLuint)mvp_mat_loc;
        state->shadow_shader.v_loc = (GLuint)v_loc;
    }

    {
        const char *vertex_shader =
            "in vec3 v;"
            "in vec3 vn;"
            "in vec2 vt;"
            "in int mtl_idx;"
            "out vec3 frag_vn;"
            "out vec2 frag_vt;"
            "out vec4 frag_light_v;"
            "flat out int frag_mtl_idx;"

            "uniform mat4 proj_view_mat;"
            "uniform mat4 model_mat;"
            "uniform mat4 shadow_proj_view_mat;"

            "void main() {"
            "   frag_vt = vt;"
            "   frag_vn = (transpose(inverse(model_mat)) * vec4(vn, 0.0)).xyz;"
            "   frag_vn = normalize(frag_vn);"
            "   frag_light_v = shadow_proj_view_mat * model_mat * vec4(v, 1.0);"
            "   frag_light_v /= frag_light_v.w;"
            "   frag_light_v += 1.0;"
            "   frag_light_v *= 0.5;"
            "   frag_mtl_idx = mtl_idx;"
            "   gl_Position = proj_view_mat * model_mat * vec4(v, 1.0);"
            "}";

        const char *fragment_shader =
            "precision highp float;"

            "in vec3 frag_vn;"
            "in vec2 frag_vt;"
            "in vec4 frag_light_v;"
            "flat in int frag_mtl_idx;"
            "out vec4 g_frag_color;"

            "uniform int texture_type;"
            "uniform int receives_shadow;"
            "uniform vec3 materials[8];"
            "uniform vec3 color;"
            "uniform vec2 uv_scale;"
            "uniform sampler2D shadow_depth_tex;"
            "uniform sampler2D diffuse_tex;"

            "void main() {"
            "   float bias = 0.01;"
            "   float shadow = 0.0;"
            "   if (frag_light_v.x >= 0.0 && frag_light_v.x <= 1.0 && frag_light_v.z >= 0.0"
            "        && frag_light_v.y >= 0.0 && frag_light_v.y <= 1.0 && frag_light_v.z <= 1.0) {"
            "      ivec2 size = textureSize(shadow_depth_tex, 0);"
            "      float size_x = float(size.x);"
            "      float size_y = float(size.y);"

            "      for(int x = -1; x <= 1; x++) {"
            "         for(int y = -1; y <= 1; y++) {"
            "            float pcf_depth = texture(shadow_depth_tex, frag_light_v.xy + vec2(float(x) / size_x, float(y) / size_y)).r; "
            "            shadow += frag_light_v.z - bias > pcf_depth ? 0.4 : 0.0;"
            "         }"
            "      }"
            "      shadow /= 9.0;"
            "   }"

            "   float pi = 3.1415;"
            "   vec3 light_dir_1 = vec3(sin(0.4 * pi) * cos(0.0 * pi), sin(0.4 * pi) * sin(0.0 * pi), cos(0.4 * pi));"
            "   vec3 light_dir_2 = vec3(sin(0.4 * pi) * cos(0.4 * pi), sin(0.4 * pi) * sin(0.4 * pi), cos(0.4 * pi));"
            "   vec3 light_dir_3 = vec3(sin(0.4 * pi) * cos(1.0 * pi), sin(0.4 * pi) * sin(1.0 * pi), cos(0.4 * pi));"
            "   vec3 light_dir_4 = vec3(sin(0.4 * pi) * cos(1.5 * pi), sin(0.4 * pi) * sin(1.5 * pi), cos(0.4 * pi));"
            "   float d = abs(dot(light_dir_1, frag_vn));"
            "   d += abs(dot(light_dir_2, frag_vn));"
            "   d += abs(dot(light_dir_3, frag_vn));"
            "   d += abs(dot(light_dir_4, frag_vn));"
            "   d *= 0.25;"
            "   vec2 diffuse_tex_coord = vec2(frag_vt.x * uv_scale.x, frag_vt.y * uv_scale.y);"
            "   vec4 diffuse_color;"
            "   if (texture_type == 0) {"
            "      vec4 c = texture(diffuse_tex, diffuse_tex_coord);"
            "      diffuse_color.xyz = c.xyz;"
            "      diffuse_color.w = c.w;"
            "   } else if (texture_type == 1) {"
            "      diffuse_color.xyz = color;"
            "      diffuse_color.w = 1.0;"
            "   } else if (texture_type == 2) {"
            "      diffuse_color.xyz = materials[frag_mtl_idx];"
            "      diffuse_color.w = 1.0;"
            "   } else if (texture_type == 3) {"
            "      vec4 c = texture(diffuse_tex, diffuse_tex_coord);"
            "      diffuse_color.xyz = color;"
            "      diffuse_color.w = c.w;"
            "   }"
            "   if (receives_shadow == 1) {"
            "      g_frag_color.xyz = (1.0 - shadow) * d * diffuse_color.xyz;"
            "   } else {"
            "      g_frag_color.xyz = diffuse_color.xyz;"
            "   }"
            "   g_frag_color.w = diffuse_color.w;"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint shadow_proj_view_mat_loc = glGetUniformLocation(program, "shadow_proj_view_mat");
        GLint proj_view_mat_loc = glGetUniformLocation(program, "proj_view_mat");
        GLint model_mat_loc = glGetUniformLocation(program, "model_mat");
        GLint texture_type_loc = glGetUniformLocation(program, "texture_type");
        GLint receives_shadow_loc = glGetUniformLocation(program, "receives_shadow");
        GLint color_loc = glGetUniformLocation(program, "color");
        GLint uv_scale_loc = glGetUniformLocation(program, "uv_scale");
        GLint shadow_depth_tex_loc = glGetUniformLocation(program, "shadow_depth_tex");
        GLint diffuse_tex_loc = glGetUniformLocation(program, "diffuse_tex");
        GLint v_loc = glGetAttribLocation(program, "v");
        GLint vn_loc = glGetAttribLocation(program, "vn");
        GLint vt_loc = glGetAttribLocation(program, "vt");
        GLint mtl_idx_loc = glGetAttribLocation(program, "mtl_idx");

        state->shader.program = (GLuint)program;
        state->shader.shadow_proj_view_mat_loc = (GLuint)shadow_proj_view_mat_loc;
        state->shader.proj_view_mat_loc = (GLuint)proj_view_mat_loc;
        state->shader.model_mat_loc = (GLuint)model_mat_loc;
        state->shader.texture_type_loc = (GLuint)texture_type_loc;
        state->shader.receives_shadow_loc = (GLuint)receives_shadow_loc;
        state->shader.color_loc = (GLuint)color_loc;
        state->shader.uv_scale_loc = (GLuint)uv_scale_loc;
        state->shader.v_loc = (GLuint)v_loc;
        state->shader.vn_loc = (GLuint)vn_loc;
        state->shader.vt_loc = (GLuint)vt_loc;
        state->shader.mtl_idx_loc = (GLuint)mtl_idx_loc;
        state->shader.shadow_depth_tex_loc = (GLuint)shadow_depth_tex_loc;
        state->shader.diffuse_tex_loc = (GLuint)diffuse_tex_loc;

        for (int i = 0; i < 16; i++) {
            char uniform_name[256];
            sprintf(uniform_name, "materials[%d]", i);
            GLint loc = glGetUniformLocation(program, uniform_name);
            state->shader.material_locs[i] = loc;
        }
    }

    {
        const char *vertex_shader =
            "in vec3 v;"
            "in vec2 vt;"
            "out vec2 frag_vt;"

            "uniform mat4 proj_view_mat;"
            "uniform mat4 model_mat;"

            "void main() {"
            "   frag_vt = vt;"
            "   gl_Position = proj_view_mat * model_mat * vec4(v, 1.0);"
            "}";

        const char *fragment_shader =
            "precision highp float;"

            "in vec2 frag_vt;"
            "out vec4 g_frag_color;"

            "uniform int texture_type;"
            "uniform vec3 color;"
            "uniform sampler2D diffuse_tex;"

            "void main() {"
            "   vec2 diffuse_tex_coord = vec2(frag_vt.x, frag_vt.y);"
            "   vec4 diffuse_color;"
            "   if (texture_type == 0) {"
            "      vec4 c = texture(diffuse_tex, diffuse_tex_coord);"
            "      diffuse_color.xyz = c.xyz;"
            "      diffuse_color.w = c.w;"
            "   } else if (texture_type == 1) {"
            "      diffuse_color.xyz = color;"
            "      diffuse_color.w = 1.0;"
            "   } else if (texture_type == 2) {"
            "      diffuse_color.xyz = vec3(1.0, 0.0, 0.0);"
            "      diffuse_color.w = 1.0;"
            "   } else if (texture_type == 3) {"
            "      vec4 c = texture(diffuse_tex, diffuse_tex_coord);"
            "      diffuse_color.xyz = color;" 
            "      diffuse_color.w = c.w;"
            "   }"
            "   g_frag_color = diffuse_color;"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);

        GLint proj_view_mat_loc = glGetUniformLocation(program, "proj_view_mat");
        GLint model_mat_loc = glGetUniformLocation(program, "model_mat");
        GLint texture_type_loc = glGetUniformLocation(program, "texture_type");
        GLint color_loc = glGetUniformLocation(program, "color");
        GLint diffuse_tex_loc = glGetUniformLocation(program, "diffuse_tex");
        GLint v_loc = glGetAttribLocation(program, "v");
        GLint vt_loc = glGetAttribLocation(program, "vt");

        state->ui_shader.program = (GLuint)program;
        state->ui_shader.proj_view_mat_loc = (GLuint)proj_view_mat_loc;
        state->ui_shader.model_mat_loc = (GLuint)model_mat_loc;
        state->ui_shader.texture_type_loc = (GLuint)texture_type_loc;
        state->ui_shader.color_loc = (GLuint)color_loc;
        state->ui_shader.v_loc = (GLuint)v_loc;
        state->ui_shader.vt_loc = (GLuint)vt_loc;
        state->ui_shader.diffuse_tex_loc = (GLuint)diffuse_tex_loc;
    }

    state->fonts["consolas_bold_12"] = load_font("consolas_bold_12", 128);
    state->fonts["consolas_bold_48"] = load_font("consolas_bold_48", 512);
}

void push_drawable(struct renderer_state *state, struct drawable drawable) {
    assert(state->num_drawables < MAX_NUM_RENDERER_DRAWABLES);
    state->drawables[state->num_drawables++] = drawable;
}

void push_ui_drawable(struct renderer_state *state, struct drawable drawable) {
    assert(state->num_ui_drawables < MAX_NUM_RENDERER_DRAWABLES);
    state->ui_drawables[state->num_ui_drawables++] = drawable;
}

static int compare_drawables(const void *a, const void *b) {
    struct drawable *da = (struct drawable *)a;
    struct drawable *db = (struct drawable *)b;
    if (da->is_transparent) {
        return 1;
    }
    if (db->is_transparent) {
        return -1;
    }
    return 0;
}

void draw_renderer_state(struct renderer_state *state) {
    mat4 proj_view_mat = mat4_multiply_n(2, state->proj_mat, state->view_mat);
    state->shadow_world_mat = mat4_look_at(V3(0.0f, 0.0f, 0.0f), 
            state->shadow_direction, state->shadow_up);
    state->shadow_view_mat = mat4_look_at(state->shadow_position, 
            vec3_add(state->shadow_position, state->shadow_direction), state->shadow_up);
    state->shadow_proj_mat = mat4_orthographic_projection(
            -state->shadow_x, state->shadow_x,
            -state->shadow_y, state->shadow_y,
            -state->shadow_z, state->shadow_z);
    mat4 shadow_proj_view_mat = mat4_multiply_n(2, state->shadow_proj_mat,
            state->shadow_view_mat);
    qsort(state->drawables, state->num_drawables, sizeof(struct drawable), compare_drawables);

    {
        glBindFramebuffer(GL_FRAMEBUFFER, state->shadow_fb);
        glViewport(0, 0, state->shadow_size, state->shadow_size);
        glClear(GL_DEPTH_BUFFER_BIT); 

        for (int i = 0; i < state->num_drawables; i++) {
            struct drawable drawable = state->drawables[i];
            struct geometry_array *geometry_array = drawable.geometry_array;

            if (!drawable.casts_shadow) {
                continue;
            }

            for (int j = 0; j < geometry_array->num_geometries; j++) {
                struct geometry *geometry = &geometry_array->geometries[j];

                mat4 model_mat = mat4_multiply_n(3,
                        drawable.model_mat,
                        geometry->transformation,
                        drawable.pre_transformation[j]);
                mat4 mvp_mat = mat4_multiply_n(2, shadow_proj_view_mat, model_mat);

                glUseProgram(state->shadow_shader.program);
                glUniformMatrix4fv(state->shadow_shader.mvp_mat_loc, 1, GL_TRUE, mvp_mat.m);

                glBindBuffer(GL_ARRAY_BUFFER, geometry->vertices_vbo);
                glVertexAttribPointer(state->shadow_shader.v_loc, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(state->shadow_shader.v_loc);

                glDrawArrays(GL_TRIANGLES, 0, 3 * geometry->num_vertices);
            }
        }
    }

    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, 1920, 1080);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        for (int i = 0; i < state->num_drawables; i++) {
            struct drawable drawable = state->drawables[i];
            struct geometry_array *geometry_array = drawable.geometry_array;

            for (int j = 0; j < geometry_array->num_geometries; j++) {
                struct geometry *geometry = &geometry_array->geometries[j];
                struct material material = drawable.material;

                mat4 model_mat = mat4_multiply_n(3,
                        drawable.model_mat,
                        geometry->transformation,
                        drawable.pre_transformation[j]);

                glUseProgram(state->shader.program);
                glUniformMatrix4fv(state->shader.shadow_proj_view_mat_loc, 1, GL_TRUE, shadow_proj_view_mat.m);
                glUniformMatrix4fv(state->shader.proj_view_mat_loc, 1, GL_TRUE, proj_view_mat.m);
                glUniformMatrix4fv(state->shader.model_mat_loc, 1, GL_TRUE, model_mat.m);

                glUniform1i(state->shader.shadow_depth_tex_loc, 0);
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, state->shadow_tex);

                if (drawable.receives_shadow) {
                    glUniform1i(state->shader.receives_shadow_loc, 1);
                } else {
                    glUniform1i(state->shader.receives_shadow_loc, 0);
                }

                switch (material.type) {
                    case MATERIAL_TYPE_COLOR: {
                        vec3 kd = material.color;
                        glUniform1i(state->shader.texture_type_loc, 1);
                        glUniform3f(state->shader.color_loc, kd.x, kd.y, kd.z);
                        break;
                    }
                    case MATERIAL_TYPE_OBJ: {
                        vec3 kd = material.obj_material.colors[geometry->material_index];
                        glUniform1i(state->shader.texture_type_loc, 1);
                        glUniform3f(state->shader.color_loc, kd.x, kd.y, kd.z);
                        break;
                    }
                    case MATERIAL_TYPE_WIREFRAME: {
                        vec3 kd = material.color;
                        glUniform1i(state->shader.texture_type_loc, 1);
                        glUniform3f(state->shader.color_loc, kd.x, kd.y, kd.z);
                        break;
                    }
                    case MATERIAL_TYPE_IMAGE: {
                        vec2 uv_scale = material.image_material.uv_scale;
                        GLuint image = material.image_material.image;
                        glUniform1i(state->shader.texture_type_loc, 0);
                        glUniform2f(state->shader.uv_scale_loc, uv_scale.x, uv_scale.y);
                        glUniform1i(state->shader.diffuse_tex_loc, 1);
                        glActiveTexture(GL_TEXTURE1);
                        glBindTexture(GL_TEXTURE_2D, image);
                        break;
                    }
                    case MATERIAL_TYPE_COLORED_IMAGE: {
                        vec3 color = material.colored_image_material.color;
                        GLuint image = material.colored_image_material.image;
                        glUniform1i(state->shader.texture_type_loc, 3);
                        glUniform2f(state->shader.uv_scale_loc, 1.0f, 1.0f);
                        glUniform3f(state->shader.color_loc, color.x, color.y, color.z);
                        glUniform1i(state->shader.diffuse_tex_loc, 1);
                        glActiveTexture(GL_TEXTURE1);
                        glBindTexture(GL_TEXTURE_2D, image);
                        break;
                    }
                }

                glBindBuffer(GL_ARRAY_BUFFER, geometry->vertices_vbo);
                glVertexAttribPointer(state->shader.v_loc, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(state->shader.v_loc);

                glBindBuffer(GL_ARRAY_BUFFER, geometry->normals_vbo);
                glVertexAttribPointer(state->shader.vn_loc, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(state->shader.vn_loc);

                glBindBuffer(GL_ARRAY_BUFFER, geometry->texture_coords_vbo);
                glVertexAttribPointer(state->shader.vt_loc, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(state->shader.vt_loc);

                if (material.type == MATERIAL_TYPE_WIREFRAME) {
                    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                }

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->faces_vbo);
                glDrawElements(GL_TRIANGLES, 3 * geometry->num_faces, GL_UNSIGNED_INT, 0);

                if (material.type == MATERIAL_TYPE_WIREFRAME) {
                    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                }
            }
        }
    }

    {
        mat4 ui_proj_mat = mat4_orthographic_projection(
                0.0f, 1920.0f,
                0.0f, 1280.0f,
                -1.0f, 1.0f);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, 1920, 1080);
        glClear(GL_DEPTH_BUFFER_BIT);

        for (int i = 0; i < state->num_ui_drawables; i++) {
            struct drawable drawable = state->ui_drawables[i];
            struct geometry_array *geometry_array = drawable.geometry_array;

            for (int j = 0; j < geometry_array->num_geometries; j++) {
                struct geometry *geometry = &geometry_array->geometries[j];
                struct material material = drawable.material;

                mat4 model_mat = mat4_multiply_n(3,
                        drawable.model_mat,
                        geometry->transformation,
                        drawable.pre_transformation[j]);

                glUseProgram(state->ui_shader.program);
                glUniformMatrix4fv(state->ui_shader.proj_view_mat_loc, 
                        1, GL_TRUE, ui_proj_mat.m);
                glUniformMatrix4fv(state->ui_shader.model_mat_loc,
                        1, GL_TRUE, model_mat.m);

                switch (material.type) {
                    case MATERIAL_TYPE_COLOR: {
                        assert(false);
                        break;
                    }
                    case MATERIAL_TYPE_OBJ: {
                        assert(false);
                        break;
                    }
                    case MATERIAL_TYPE_WIREFRAME: {
                        assert(false);
                        break;
                    }
                    case MATERIAL_TYPE_IMAGE: {
                        assert(false);
                        break;
                    }
                    case MATERIAL_TYPE_COLORED_IMAGE: {
                        vec3 color = material.colored_image_material.color;
                        GLuint image = material.colored_image_material.image;
                        glUniform1i(state->ui_shader.texture_type_loc, 3);
                        glUniform3f(state->ui_shader.color_loc, color.x, color.y, color.z);
                        glUniform1i(state->ui_shader.diffuse_tex_loc, 0);
                        glActiveTexture(GL_TEXTURE0);
                        glBindTexture(GL_TEXTURE_2D, image);
                        break;
                    }
                }

                glBindBuffer(GL_ARRAY_BUFFER, geometry->vertices_vbo);
                glVertexAttribPointer(state->ui_shader.v_loc, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(state->ui_shader.v_loc);

                glBindBuffer(GL_ARRAY_BUFFER, geometry->texture_coords_vbo);
                glVertexAttribPointer(state->ui_shader.vt_loc, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(state->ui_shader.vt_loc);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->faces_vbo);
                glDrawElements(GL_TRIANGLES, 3 * geometry->num_faces, GL_UNSIGNED_INT, 0);
            }
        }
    }

    state->num_drawables = 0;
    state->num_ui_drawables = 0;
}

struct drawable create_drawable(void) {
    struct drawable drawable;
    drawable.is_transparent = false;
    drawable.casts_shadow = false;
    drawable.receives_shadow = true;
    drawable.model_mat = mat4_identity();
    for (int i = 0; i < GEOMETRY_ARRAY_MAX_GEOMETRIES; i++) {
        drawable.pre_transformation[i] = mat4_identity();
    }
    drawable.geometry_array = NULL;
    return drawable;
}

struct material color_material(vec3 color) {
    struct material material;
    material.type = MATERIAL_TYPE_COLOR;
    material.color = color;
    return material;
}

struct material image_material(vec2 uv_scale, GLuint image) {
    struct material material;    
    material.type = MATERIAL_TYPE_IMAGE;
    material.image_material.uv_scale = uv_scale;
    material.image_material.image = image;
    return material;
}

struct material colored_image_material(vec3 color, GLuint image) {
    struct material material;    
    material.type = MATERIAL_TYPE_COLORED_IMAGE;
    material.colored_image_material.color = color;
    material.colored_image_material.image = image;
    return material;
}

struct material wireframe_material(vec3 color) {
    struct material material;
    material.type = MATERIAL_TYPE_WIREFRAME;
    material.color = color;
    return material;
}

struct geometry_face create_geometry_face(int f0, int f1, int f2) {
    struct geometry_face face;
    face.f0 = f0;
    face.f1 = f1;
    face.f2 = f2;
    return face;
}

struct geometry_array *get_geometry_array(struct renderer_state *renderer, const char *name) {
    assert(renderer->geometry_arrays.count(std::string(name)) > 0);
    return renderer->geometry_arrays[std::string(name)];
}

void update_text_geometry_array(struct renderer_state *renderer, struct font font,
        struct geometry_array *geometry_array, const char *text) {
    geometry_array->num_geometries = 1;

    int n = strlen(text);

    struct geometry *geometry = &geometry_array->geometries[0]; 
    geometry->transformation = mat4_identity();
    geometry->material_index = -1;
    geometry->num_vertices = 6 * n;
    geometry->num_faces = 2 * n;

    int cur_x = 0,
        cur_y = 0,
        max_height = 0;
    for (int i = 0; i < n; i++) {
        int c = text[i];

        float x0 = cur_x + font.chars[c].x_off;
        float x1 = x0 + font.chars[c].w;
        float y0 = cur_y - font.chars[c].y_off;
        float y1 = y0 - font.chars[c].h;

        float u0 = font.chars[c].x / (float) font.tex_width;
        float u1 = u0 + font.chars[c].w / (float) font.tex_width;
        float v0 = font.chars[c].y / (float) font.tex_width;
        float v1 = v0 + font.chars[c].h / (float) font.tex_width;

        cur_x += font.chars[c].x_adv;

        geometry->vertices[6 * i + 0] = V3(x0, y0, 0.0f);
        geometry->vertices[6 * i + 1] = V3(x0, y1, 0.0f);
        geometry->vertices[6 * i + 2] = V3(x1, y1, 0.0f);
        geometry->vertices[6 * i + 3] = V3(x0, y0, 0.0f);
        geometry->vertices[6 * i + 4] = V3(x1, y1, 0.0f);
        geometry->vertices[6 * i + 5] = V3(x1, y0, 0.0f);

        geometry->transformed_vertices[6 * i + 0] = V3(x0, y0, 0.0f);
        geometry->transformed_vertices[6 * i + 1] = V3(x0, y1, 0.0f);
        geometry->transformed_vertices[6 * i + 2] = V3(x1, y1, 0.0f);
        geometry->transformed_vertices[6 * i + 3] = V3(x0, y0, 0.0f);
        geometry->transformed_vertices[6 * i + 4] = V3(x1, y1, 0.0f);
        geometry->transformed_vertices[6 * i + 5] = V3(x1, y0, 0.0f);

        geometry->normals[6 * i + 0] = V3(0.0f, 0.0f, 1.0f);
        geometry->normals[6 * i + 1] = V3(0.0f, 0.0f, 1.0f);
        geometry->normals[6 * i + 2] = V3(0.0f, 0.0f, 1.0f);
        geometry->normals[6 * i + 3] = V3(0.0f, 0.0f, 1.0f);
        geometry->normals[6 * i + 4] = V3(0.0f, 0.0f, 1.0f);
        geometry->normals[6 * i + 5] = V3(0.0f, 0.0f, 1.0f);

        geometry->texture_coords[6 * i + 0] = V2(u0, v0);
        geometry->texture_coords[6 * i + 1] = V2(u0, v1);
        geometry->texture_coords[6 * i + 2] = V2(u1, v1);
        geometry->texture_coords[6 * i + 3] = V2(u0, v0);
        geometry->texture_coords[6 * i + 4] = V2(u1, v1);
        geometry->texture_coords[6 * i + 5] = V2(u1, v0);

        geometry->faces[2 * i + 0] = create_geometry_face(6 * i + 0, 6 * i + 1, 6 * i + 2);
        geometry->faces[2 * i + 1] = create_geometry_face(6 * i + 3, 6 * i + 4, 6 * i + 5);

        if (font.chars[c].h > max_height) {
            max_height = font.chars[c].h;
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, geometry->vertices_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * geometry->num_vertices,
            geometry->vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, geometry->normals_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * geometry->num_vertices,
            geometry->normals, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, geometry->texture_coords_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * geometry->num_vertices,
            geometry->texture_coords, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->faces_vbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(struct geometry_face) * geometry->num_faces, 
            geometry->faces, GL_STATIC_DRAW);
}

void init_text_geometry_array(struct renderer_state *renderer, 
        struct geometry_array *geometry_array) {
    geometry_array->num_geometries = 1;
    struct geometry *geometry = &geometry_array->geometries[0]; 
    glGenBuffers(1, &geometry->vertices_vbo);
    glGenBuffers(1, &geometry->normals_vbo);
    glGenBuffers(1, &geometry->texture_coords_vbo);
    glGenBuffers(1, &geometry->faces_vbo);
}

struct material get_material(struct renderer_state *renderer, const char *name) {
    assert(renderer->materials.count(std::string(name)) > 0);
    return renderer->materials[std::string(name)];
}

void push_drawable_cube(struct renderer_state *renderer, vec3 color, vec3 p, float s) {
    struct drawable drawable = create_drawable();
    drawable.geometry_array = get_geometry_array(renderer, "cube");
    drawable.material = color_material(color);
    drawable.casts_shadow = false;
    drawable.model_mat = mat4_multiply(mat4_translation(p), mat4_scale(V3(s, s, s)));
    for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
        drawable.pre_transformation[i] = mat4_identity();
    }
    push_drawable(renderer, drawable);
}

void push_drawable_line(struct renderer_state *renderer, vec3 color, vec3 p0, vec3 p1, float s) {
    struct drawable drawable = create_drawable();
    drawable.geometry_array = get_geometry_array(renderer, "cube");
    drawable.material = color_material(color);
    drawable.casts_shadow = false;
    drawable.model_mat = mat4_cube_to_line_transform(p0, p1, 0.05f);
    for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
        drawable.pre_transformation[i] = mat4_identity();
    }
    push_drawable(renderer, drawable);
}
