#ifndef _DRAWING_H
#define _DRAWING_H

#include <map>
#include <string>

#include "maths/maths.h"
#include "GL/GL.h"
#include "font.h"

#define MAX_NUM_RENDERER_DRAWABLES 128
#define MAX_NUM_MESH_GEOMETRIES 16
#define MAX_NUM_OBJ_MATERIALS 16 
#define GEOMETRY_MAX_VERTICES 1024
#define GEOMETRY_ARRAY_MAX_GEOMETRIES 16

enum material_type {
    MATERIAL_TYPE_WIREFRAME,
    MATERIAL_TYPE_COLOR,
    MATERIAL_TYPE_OBJ,
    MATERIAL_TYPE_IMAGE,
    MATERIAL_TYPE_COLORED_IMAGE,
};

struct material {
    enum material_type type;
    union {
        vec3 color;
        struct {
            int num_materials;
            vec3 colors[GEOMETRY_ARRAY_MAX_GEOMETRIES];
        } obj_material;
        struct {
            GLuint image; 
            vec2 uv_scale;
        } image_material;
        struct {
            vec3 color;
            GLuint image;
        } colored_image_material;
    };
};

struct geometry_face {
    int f0, f1, f2;
};

struct geometry {
    mat4 transformation;
    int material_index;

    int num_vertices;
    vec3 vertices[3 * GEOMETRY_MAX_VERTICES];
    vec3 transformed_vertices[3 * GEOMETRY_MAX_VERTICES];
    vec3 normals[3 * GEOMETRY_MAX_VERTICES];
    vec2 texture_coords[3 * GEOMETRY_MAX_VERTICES];

    int num_faces;
    struct geometry_face faces[3 * GEOMETRY_MAX_VERTICES];

    GLuint vertices_vbo;
    GLuint normals_vbo;
    GLuint texture_coords_vbo;
    GLuint faces_vbo;
};

struct geometry_array {
    int num_geometries;
    struct geometry geometries[GEOMETRY_ARRAY_MAX_GEOMETRIES];
};

struct drawable {
    bool is_transparent;
    bool casts_shadow, receives_shadow;
    mat4 model_mat;
    mat4 pre_transformation[GEOMETRY_ARRAY_MAX_GEOMETRIES];
    struct material material;
    struct geometry_array *geometry_array;
};

struct renderer_state {
    mat4 proj_mat, view_mat;
    mat4 ui_proj_mat, ui_view_mat;
    mat4 car_proj_mat, car_view_mat;

    struct {
        GLuint program;
        GLuint shadow_proj_view_mat_loc, 
               proj_view_mat_loc, 
               model_mat_loc, 
               texture_type_loc,
               receives_shadow_loc,
               color_loc, 
               uv_scale_loc;
        GLuint v_loc, vn_loc, vt_loc, mtl_idx_loc;
        GLuint shadow_depth_tex_loc, diffuse_tex_loc;
        GLuint material_locs[16];
    } shader;

    struct {
        GLuint program;
        GLuint mvp_mat_loc;
        GLuint v_loc;
    } shadow_shader;

    struct {
        GLuint program;
        GLuint proj_view_mat_loc, 
               model_mat_loc, 
               texture_type_loc,
               color_loc, 
               diffuse_tex_loc;
        GLuint v_loc, vt_loc;
    } ui_shader;

    int shadow_size;
    GLuint shadow_fb, shadow_tex;
    mat4 shadow_proj_mat, shadow_view_mat, shadow_world_mat;
    vec3 shadow_position, shadow_direction, shadow_up;
    float shadow_x, shadow_y, shadow_z;

    GLuint floor_tex;

    int num_drawables;
    struct drawable drawables[MAX_NUM_RENDERER_DRAWABLES];

    int num_ui_drawables;
    struct drawable ui_drawables[MAX_NUM_RENDERER_DRAWABLES];
    
    std::map<std::string, struct geometry_array *> geometry_arrays;
    std::map<std::string, struct material> materials;
    std::map<std::string, struct font> fonts;
};

struct geometry_array *get_geometry_array(struct renderer_state *renderer, const char *name);
struct material get_material(struct renderer_state *renderer, const char *name);

struct drawable create_drawable(void);
struct material color_material(vec3 color); 
struct material image_material(vec2 uv_scale, GLuint image);
struct material colored_image_material(vec3 color, GLuint image);
struct material wireframe_material(vec3 color);
struct geometry_face create_geometry_face(int f0, int f1, int f2);

GLuint load_texture(const char *filename, int filter);
int load_dae(const char *filename, struct geometry_array *geometry_array, struct material *material);

void init_renderer_state(struct renderer_state *state);
void draw_renderer_state(struct renderer_state *state);
void push_drawable(struct renderer_state *state, struct drawable drawable);
void push_drawable_cube(struct renderer_state *renderer, vec3 color, vec3 p, float s);
void push_drawable_line(struct renderer_state *renderer, vec3 color, vec3 p0, vec3 p1, float s);
void push_ui_drawable(struct renderer_state *state, struct drawable drawable);
mat4 cube_to_line_model_mat(vec3 p0, vec3 p1, float s);

void update_text_geometry_array(struct renderer_state *renderer, struct font font,
        struct geometry_array *geometry_array, const char *text);
void init_text_geometry_array(struct renderer_state *renderer,
        struct geometry_array *geometry_array);

#endif
