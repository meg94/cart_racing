#include <assert.h>
#include <stdlib.h>
#include <string>

#include "entity.h"

void init_entity_manager(struct entity_manager *manager) {
    manager->cur_idx = 1; 
    manager->num_free = 0; 
}

struct entity_id allocate_entity(struct entity_manager *manager) {
    struct entity_id id;

    if (manager->num_free == 0) {
        id.gen = 0;
        id.idx = manager->cur_idx;
        manager->gen_array[id.idx] = 0;
        manager->cur_idx++;
        manager->active_array[id.idx] = true;
    } else {
        int num_free = --manager->num_free;
        uint32_t idx = manager->free_array[num_free];
        id.gen = manager->gen_array[idx];
        id.idx = idx;
        manager->active_array[id.idx] = true;
    }

    struct entity *entity = get_entity_from_id(manager, id);
    entity->id = id;

    return id;
}

void delete_entity(struct entity_manager *manager, struct entity_id id) {
    uint32_t cur_gen = manager->gen_array[id.idx];
    if (id.idx == 0 || id.gen != cur_gen) {
        return;
    }

    manager->gen_array[id.idx]++;
    manager->active_array[id.idx] = false;
    manager->free_array[manager->num_free++] = id.idx;
}

struct entity *get_entity_from_id(struct entity_manager *manager, struct entity_id id) {
    uint32_t cur_gen = manager->gen_array[id.idx];
    if (id.idx == 0 || cur_gen != id.gen) {
        return NULL;
    }

    return &manager->entity_array[id.idx];
}

void init_entity(struct entity *entity) {
    //entity->model = NULL; 
    entity->collider = NULL;
    entity->position = V3(0.0f, 0.0f, 0.0f);
    entity->scale = V3(1.0f, 1.0f, 1.0f);
    entity->rotation = quat_create_from_axis_angle(V3(1.0f, 0.0f, 0.0f), 0.0f);
}

void set_entity_position(struct entity *entity, vec3 position) {
    entity->position = position;
    quat rotation = entity->rotation; 
    set_collider_transform(entity->collider, position, rotation);
}

void set_entity_rotation(struct entity *entity, quat rotation) {
    vec3 position = entity->position;
    entity->rotation = rotation;
    set_collider_transform(entity->collider, position, rotation);
}

void set_entity_scale(struct entity *entity, vec3 scale) {
    entity->scale = scale;
    set_collider_scale(entity->collider, scale);
}

vec3 local_entity_pos_to_world(struct entity *entity, vec3 local) {
    mat4 transformation = mat4_multiply_n(2,
            mat4_translation(entity->position),
            mat4_from_quat(entity->rotation));
    return vec3_apply_mat4(local, 1.0f, transformation);
}

void init_car_entity(struct game_state *state, struct entity *car) {
    struct physics_state *physics = state->physics_state;

    vec3 s = V3(0.7f, 0.3f, 1.0f);

    init_entity(car);
    car->type = ENTITY_TYPE_CAR;
    car->scale = V3(2.0f, 2.0f, 2.0f);
    car->position = V3(0.0f, 0.0f, 0.0f);
    car->collider = create_collider(physics, create_box_collider_shape(s), car->id, 1.0f);

    car->car.is_user_controlled = true;
    car->car.wheel_rotation = 0.0f;
    car->car.mesh_pos = V3(0.0f, -0.27f, -0.07f);

    s = V3(0.58f, 0.3f, 0.8f);

    car->car.wheel[0].local_position = V3(s.x, 0.0f, s.z);
    car->car.wheel[0].hit_position = V3(0.0f, 0.0f, 0.0f);
    car->car.wheel[0].hit_normal = V3(0.0f, 1.0f, 0.0f);
    car->car.wheel[0].hit_fraction = 0.0f;

    car->car.wheel[1].local_position = V3(s.x, 0.0f, -s.z);
    car->car.wheel[1].hit_position = V3(0.0f, 0.0f, 0.0f);
    car->car.wheel[1].hit_normal = V3(0.0f, 1.0f, 0.0f);
    car->car.wheel[1].hit_fraction = 0.0f;

    car->car.wheel[2].local_position = V3(-s.x, 0.0f, s.z);
    car->car.wheel[2].hit_position = V3(0.0f, 0.0f, 0.0f);
    car->car.wheel[2].hit_normal = V3(0.0f, 1.0f, 0.0f);
    car->car.wheel[2].hit_fraction = 0.0f;

    car->car.wheel[3].local_position = V3(-s.x, 0.0f, -s.z);
    car->car.wheel[3].hit_position = V3(0.0f, 0.0f, 0.0f);
    car->car.wheel[3].hit_normal = V3(0.0f, 1.0f, 0.0f);
    car->car.wheel[3].hit_fraction = 0.0f;

    set_entity_position(car, car->position);
}


static void car_entity_raytrace_wheel(struct game_state *game_state, struct entity *car, int i) {
    struct physics_state *physics = game_state->physics_state;

    vec3 from = car->car.wheel[i].local_position;
    vec3 to = vec3_add(from, V3(0.0f, -0.65f, 0.0f));
    vec3 from_world = local_entity_pos_to_world(car, from);
    vec3 to_world = local_entity_pos_to_world(car, to);

    struct ray_test_result result = do_ray_test(physics, from_world, to_world);
    if (result.hit) {
        float hit_fraction = result.t;
        vec3 hit_position = result.position;
        vec3 hit_normal = result.normal;

        car->car.wheel[i].hit_fraction = hit_fraction;
        car->car.wheel[i].hit_normal = hit_normal;
        car->car.wheel[i].hit_position = hit_position;
        
        float alpha = 25.0f * (1.0f - hit_fraction);
        vec3 force = vec3_scale(hit_normal, alpha);
        vec3 rel_pos = vec3_subtract(from_world, car->position);
        apply_force_to_collider(car->collider, force, rel_pos);

        vec3 velocity = get_collider_velocity_at_point(car->collider, from_world);
        float damping = sqrt(25.0f) * vec3_dot(velocity, hit_normal);
        vec3 damping_force = vec3_scale(hit_normal, -damping);
        apply_force_to_collider(car->collider, damping_force, rel_pos);
    } else {
        car->car.wheel[i].hit_fraction = 1.0f;
    }
}

void update_car_entity(struct game_state *state, struct entity *car, float dt) {
    vec3 car_velocity;

    {
        activate_collider(car->collider);
        car->position = get_collider_position(car->collider);
        car->rotation = get_collider_rotation(car->collider);
        car_velocity = get_collider_velocity(car->collider);
    }

    int wheels_on_ground = 0;
    for (int i = 0; i < 4; i++) {
        if (car->car.wheel[i].hit_fraction < 1.0f) {
            wheels_on_ground++;
        }
    }

    if (wheels_on_ground > 0) {
        mat4 basis = mat4_from_quat(car->rotation); 
        vec3 dir = V3(basis.m[2], basis.m[6], basis.m[10]);
        car->car.heading = acosf(dir.x);
        if (dir.z < 0.0f) {
            car->car.heading *= -1.0f;
        }

        vec3 v = car_velocity;
        v.y = 0.0f;
        v = vec3_normalize(v);

        vec3 impulse = V3(-10.0f * v.x, 0.0f, -10.0f * v.z);
        vec3 rel_pos = V3(0.0f, 0.0f, 0.0f);
        apply_force_to_collider(car->collider, impulse, rel_pos);
    }

    {
        vec3 ang_vel = get_collider_angular_velocity(car->collider);
        vec3 torque = vec3_scale(ang_vel, -2.0f);
        apply_torque_to_collider(car->collider, torque);
    }

    if (wheels_on_ground > 0) {
        mat4 basis = mat4_from_quat(car->rotation); 
        vec3 dir = V3(basis.m[2], basis.m[6], basis.m[10]);
        vec3 v = car_velocity;
        v = vec3_perpindicular_component(v, dir);
        v = vec3_scale(v, -0.01f);
        vec3 rel_pos = V3(0.0f, 0.0f, 0.0f);
        apply_impulse_to_collider(car->collider, v, rel_pos);
    }

    if (car->car.is_user_controlled) {
        if (state->input.key_clicked[GLFW_KEY_Q]) {
            vec3 force = V3(0.0f, 10.0f, 0.0f);
            vec3 rel_pos = V3(0.01f, 0.0f, 0.02f);
            apply_impulse_to_collider(car->collider, force, rel_pos);
        }

        mat4 basis = mat4_from_quat(car->rotation);
        vec3 dir = V3(basis.m[2], basis.m[6], basis.m[10]);
        vec3 pos_local = V3(0.0f, -0.1f, 0.8f);
        vec3 pos_world = local_entity_pos_to_world(car, pos_local);
        vec3 rel_pos = vec3_subtract(pos_world, car->position);

        for (int i = 0; i < 4; i++) {
            bool wheel_on_ground = car->car.wheel[i].hit_fraction < 1.0f;
            if (!wheel_on_ground) {
                continue;
            }

            vec3 force = vec3_perpindicular_component(dir, car->car.wheel[i].hit_normal);
            if (state->input.key_down[GLFW_KEY_W]) {
                force = vec3_scale(force, -0.25f * 15.0f);
                apply_force_to_collider(car->collider, force, rel_pos);
            }
            if (state->input.key_down[GLFW_KEY_S]) {
                force = vec3_scale(force, 0.25f * 15.0f);
                apply_force_to_collider(car->collider, force, rel_pos);
            }
        }

        car->car.wheel_rotation -= 0.05f * car->car.wheel_rotation;

        float speed = vec3_length(car_velocity);
        if (vec3_dot(car_velocity, dir) > 0.0f) {
            speed *= -1.0f;
        }

        if (state->input.key_down[GLFW_KEY_A]) {
            float f = speed * sin(car->car.wheel_rotation) / 2.0f;
            vec3 torque = V3(0.0f, f, 0.0f);
            apply_torque_to_collider(car->collider, torque);

            if (car->car.wheel_rotation < 0.6f) {
                car->car.wheel_rotation += 0.05f; 
            }
        }
        if (state->input.key_down[GLFW_KEY_D]) {
            float f = speed * sin(car->car.wheel_rotation) / 2.0f;
            vec3 torque = V3(0.0f, f, 0.0f);
            apply_torque_to_collider(car->collider, torque);

            if (car->car.wheel_rotation > -0.6f) {
                car->car.wheel_rotation -= 0.05f; 
            }
        }
    }

    {
        car_entity_raytrace_wheel(state, car, 0);
        car_entity_raytrace_wheel(state, car, 1);
        car_entity_raytrace_wheel(state, car, 2);
        car_entity_raytrace_wheel(state, car, 3);
    }
}

void draw_car_entity(struct game_state *state, struct entity *car) {
    struct renderer_state *renderer = state->renderer_state;
    mat4 model_mat = mat4_multiply_n(3,
            mat4_translation(car->position),
            mat4_from_quat(car->rotation),
            mat4_scale(car->scale));

    {
        struct drawable drawable = create_drawable();
        drawable.geometry_array = get_geometry_array(renderer, "raceCarGreen");
        drawable.material = get_material(renderer, "raceCarGreen");
        drawable.casts_shadow = true;
        drawable.model_mat = model_mat;

        vec3 mesh_pos = car->car.mesh_pos;
        for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
            if (i == 7 || i == 8 || i == 9 || i == 10) {
                drawable.pre_transformation[i] = mat4_multiply(
                        mat4_translation(mesh_pos),
                        mat4_rotation_y(car->car.wheel_rotation));
            } else {
                drawable.pre_transformation[i] = mat4_translation(mesh_pos);
            }
        }

        push_drawable(renderer, drawable);
    }
}

void init_track_entity(struct game_state *state, struct entity *track,
        const char *track_type_name) {
    init_entity(track);
    track->type = ENTITY_TYPE_TRACK;
    track->scale = V3(10.0f, 10.0f, 10.0f);
    track->position = V3(0.0f, 0.0f, 0.0f);
    set_track_entity_type(state, track, track_type_name);
}

void set_track_entity_type(struct game_state *state, struct entity *track,
        const char *track_type_name) {
    struct physics_state *physics = state->physics_state;

    if (track->collider) {
        delete_collider(state->physics_state, track->collider);
    }

    struct collider_shape *shape = get_collider_shape(physics, track_type_name);
    track->collider = create_collider(physics, shape, track->id, 0.0f); 
    track->track.type_name = track_type_name;

    set_entity_position(track, track->position);
}

void update_track_entity(struct game_state *state, struct entity *track, float dt) {
}

void delete_track_entity(struct game_state *state, struct entity *track) {
    if (track->collider) {
        delete_collider(state->physics_state, track->collider);
    } 
    delete_entity(state->entity_manager, track->id);
}

void draw_track_entity(struct game_state *state, struct entity *track) {
    struct renderer_state *renderer = state->renderer_state;
    const char *name = track->track.type_name;

    struct drawable drawable = create_drawable();
    drawable.geometry_array = get_geometry_array(renderer, name);
    drawable.material = get_material(renderer, name);
    drawable.casts_shadow = true;
    drawable.model_mat = mat4_multiply_n(3,
                mat4_translation(track->position),
                mat4_scale(track->scale),
                mat4_from_quat(track->rotation));
    for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
        drawable.pre_transformation[i] = mat4_identity();
    }
    push_drawable(renderer, drawable);
}

void init_floor_entity(struct game_state *state, struct entity *floor) {
    struct physics_state *physics = state->physics_state;

    init_entity(floor);
    floor->type = ENTITY_TYPE_FLOOR;
    floor->scale = V3(500.0f, 0.001f, 500.0f);
    floor->position = V3(0.0f, 0.0f, 0.0f);

    struct collider_shape *shape = create_plane_collider_shape(V3(0.0f, 1.0f, 0.0f), 0.0f);
    floor->collider = create_collider(physics, shape, floor->id, 0.0f);

    set_entity_position(floor, floor->position);
}

void update_floor_entity(struct game_state *state, struct entity *floor, float dt) {

}

void draw_floor_entity(struct game_state *state, struct entity *floor) {
    struct renderer_state *renderer = state->renderer_state;

    struct drawable drawable = create_drawable();
    drawable.geometry_array = get_geometry_array(renderer, "cube");
    drawable.material = image_material(V2(500.0f, 500.0f), renderer->floor_tex);
    drawable.casts_shadow = false;
    drawable.model_mat = mat4_multiply_n(2,
                mat4_translation(floor->position),
                mat4_scale(vec3_scale(floor->scale, 2.0f)));
    for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
        drawable.pre_transformation[i] = mat4_identity();
    }
    push_drawable(renderer, drawable);
}

void init_checkpoint_entity(struct game_state *state, struct entity *checkpoint) {
    struct physics_state *physics = state->physics_state;
    struct renderer_state *renderer = state->renderer_state;

    init_entity(checkpoint);

    vec3 s = V3(1.0f, 1.0f, 0.1f);

    checkpoint->type = ENTITY_TYPE_CHECKPOINT;
    checkpoint->scale = s;
    checkpoint->position = V3(0.0f, 0.0f, 0.0f);
    checkpoint->collider = create_collider(physics,
            create_box_collider_shape(s),
            checkpoint->id, 0.0f);
    set_collider_contact_response(checkpoint->collider, true);

    checkpoint->checkpoint.num = state->num_checkpoints++;
    checkpoint->checkpoint.model_rotation = 0.0f;

    checkpoint->checkpoint.text = new struct geometry_array();
    init_text_geometry_array(renderer, checkpoint->checkpoint.text);
}

void update_checkpoint_entity(struct game_state *state, struct entity *checkpoint, float dt) {
    if (state->cur_checkpoint != checkpoint->checkpoint.num) {
        return;
    }
    checkpoint->checkpoint.model_rotation += 1.0f * dt;
    
    struct entity *car = get_entity_from_id(state->entity_manager, state->user_car_id);
    assert(car);
    
    if (do_colliders_collide(state->physics_state, car->collider, checkpoint->collider)) {
        if (state->cur_checkpoint == 0) {
            if (state->is_first_lap) {
                state->is_first_lap = false;
                state->fastest_lap_time = state->current_lap_time;
            }
            if (state->current_lap_time < state->fastest_lap_time) {
                state->fastest_lap_time = state->current_lap_time;
            }
            state->current_lap_time = 0.0f;
        }

        state->cur_checkpoint++;
        if (state->cur_checkpoint >= state->num_checkpoints) {
            state->cur_checkpoint = 0;
        }
    }
}

void draw_checkpoint_entity(struct game_state *state, struct entity *checkpoint) {
    struct renderer_state *renderer = state->renderer_state;

    if (state->cur_checkpoint != checkpoint->checkpoint.num) {
        return;
    }

    /*
    {
        mat4 model_mat = mat4_multiply_n(3,
                mat4_translation(checkpoint->position),
                mat4_from_quat(checkpoint->rotation),
                mat4_scale(checkpoint->scale));

        struct drawable drawable = create_drawable();
        drawable.geometry_array = get_geometry_array(renderer, "cube");
        drawable.material = color_material(V3(1.0f, 1.0f, 1.0f));
        drawable.casts_shadow = false;
        drawable.model_mat = model_mat;
        for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
            drawable.pre_transformation[i] = mat4_identity();
        }

        if (state->cur_checkpoint == checkpoint->checkpoint.num) {
            drawable.material = color_material(V3(0.0f, 1.0f, 0.0f));
        }

        push_drawable(renderer, drawable);
    }
    */

    /*
    {
        vec3 s = V3(0.1f, 0.5f, 0.1f);
        vec3 p = vec3_add(checkpoint->position, V3(0.0f, -2.0f, 0.0f));

        mat4 model_mat = mat4_multiply_n(3,
                mat4_translation(p),
                mat4_rotation_y(checkpoint->checkpoint.model_rotation),
                mat4_scale(s));

        struct drawable drawable = create_drawable();
        drawable.geometry_array = get_geometry_array(renderer, "cube");
        drawable.material = color_material(V3(1.0f, 1.0f, 1.0f));
        drawable.casts_shadow = false;
        drawable.model_mat = model_mat;
        for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
            drawable.pre_transformation[i] = mat4_identity();
        }

        if (state->cur_checkpoint == checkpoint->checkpoint.num) {
            drawable.material = color_material(V3(0.0f, 1.0f, 0.0f));
        }

        push_drawable(renderer, drawable);
    }

    {
        vec3 s = V3(0.5f, 0.1f, 0.1f);
        vec3 p = vec3_add(checkpoint->position, V3(0.0f, -2.0f, 0.0f));

        mat4 model_mat = mat4_multiply_n(3,
                mat4_translation(p),
                mat4_rotation_y(checkpoint->checkpoint.model_rotation),
                mat4_scale(s));

        struct drawable drawable = create_drawable();
        drawable.geometry_array = get_geometry_array(renderer, "cube");
        drawable.material = color_material(V3(1.0f, 1.0f, 1.0f));
        drawable.casts_shadow = false;
        drawable.model_mat = model_mat;
        for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
            drawable.pre_transformation[i] = mat4_identity();
        }

        if (state->cur_checkpoint == checkpoint->checkpoint.num) {
            drawable.material = color_material(V3(0.0f, 1.0f, 0.0f));
        }

        push_drawable(renderer, drawable);
    }
    */

    {
        char text[256];
        if (checkpoint->checkpoint.num == 0) {
            sprintf(text, "%d / %d", state->num_checkpoints, state->num_checkpoints);
        } else {
            sprintf(text, "%d / %d", checkpoint->checkpoint.num, state->num_checkpoints);
        }
        struct font font = renderer->fonts["consolas_bold_48"];
        struct geometry_array *geometry_array = checkpoint->checkpoint.text;
        update_text_geometry_array(renderer, font, geometry_array, text);

        struct drawable drawable = create_drawable();
        drawable.is_transparent = true;
        drawable.geometry_array = geometry_array;
        drawable.material = colored_image_material(V3(0.0f, 0.8f, 0.0f), font.tex);
        drawable.casts_shadow = false;
        drawable.receives_shadow = false;
        drawable.model_mat = mat4_multiply_n(4, 
                mat4_translation(checkpoint->position),
                mat4_from_quat(checkpoint->rotation),
                mat4_translation(V3(-1.7f, -0.5f, 0.0f)),
                mat4_scale(V3(0.02f, 0.02f, 0.02f)));
        drawable.pre_transformation[0] = mat4_identity();
        push_drawable(renderer, drawable);
    }
}

void delete_checkpoint_entity(struct game_state *state, struct entity *checkpoint) {
    if (checkpoint->collider) {
        delete_collider(state->physics_state, checkpoint->collider);
    } 
    delete_entity(state->entity_manager, checkpoint->id);
    state->num_checkpoints--;
}

void init_extra_entity(struct game_state *state, struct entity *extra, const char *model_name) {
    struct physics_state *physics = state->physics_state;
    init_entity(extra);

    vec3 s = V3(10.0f, 10.0f, 10.0f);

    extra->type = ENTITY_TYPE_EXTRA;
    extra->scale = s;
    extra->position = V3(0.0f, 0.0f, 0.0f);
    extra->collider = create_collider(physics,
            get_collider_shape(physics, model_name),
            extra->id, 0.0f);

    extra->extra.model_name = model_name; 
}

void update_extra_entity(struct game_state *state, struct entity *extra, float dt) {

}

void draw_extra_entity(struct game_state *state, struct entity *extra) {
    struct renderer_state *renderer = state->renderer_state;

    {
        mat4 model_mat = mat4_multiply_n(3,
                mat4_translation(extra->position),
                mat4_from_quat(extra->rotation),
                mat4_scale(extra->scale));

        struct drawable drawable = create_drawable();
        drawable.geometry_array = get_geometry_array(renderer, extra->extra.model_name);
        drawable.material = get_material(renderer, extra->extra.model_name);
        drawable.casts_shadow = true;
        drawable.model_mat = model_mat;
        for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
            drawable.pre_transformation[i] = mat4_identity();
        }

        push_drawable(renderer, drawable);
    }  
}

void delete_extra_entity(struct game_state *state, struct entity *extra) {
    if (extra->collider) {
        delete_collider(state->physics_state, extra->collider);
    } 
    delete_entity(state->entity_manager, extra->id);
}
