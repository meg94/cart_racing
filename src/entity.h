#ifndef _ENTITY_H
#define _ENTITY_H

#include <stdint.h>

struct entity_id {
    uint32_t idx, gen; 
};

#include "maths/maths.h"
#include "physics.h"
#include "drawing.h"

#define MAX_NUM_ENTITIES 1000

enum entity_type {
    ENTITY_TYPE_CAR,
    ENTITY_TYPE_TRACK,
    ENTITY_TYPE_FLOOR,
    ENTITY_TYPE_CHECKPOINT,
    ENTITY_TYPE_EXTRA,
    ENTITY_NUM_TYPES,
};

struct entity {
    enum entity_type type;
    struct entity_id id;
    struct collider *collider;

    vec3 position;
    vec3 scale;
    quat rotation;

    union {
        struct {
            bool is_user_controlled;
            float heading;
            float wheel_rotation;
            vec3 mesh_pos;

            struct {
                vec3 local_position;
                vec3 hit_position;
                vec3 hit_normal;
                float hit_fraction;
            } wheel[4];
        } car;
        struct {
            const char *type_name;
        } track;
        struct {
            int num;
            float model_rotation;
            struct geometry_array *text;
        } checkpoint;
        struct {
            const char *model_name;
        } extra;
    };
};

struct entity_manager {
    int cur_idx;
    int num_free;
    uint32_t free_array[MAX_NUM_ENTITIES];
    uint32_t gen_array[MAX_NUM_ENTITIES];
    struct entity entity_array[MAX_NUM_ENTITIES];
    bool active_array[MAX_NUM_ENTITIES];
};

void init_entity_manager(struct entity_manager *manager);
struct entity_id allocate_entity(struct entity_manager *manager);
void delete_entity(struct entity_manager *manager, struct entity_id id);
struct entity *get_entity_from_id(struct entity_manager *manager, struct entity_id id);

void init_entity(struct entity *entity);
void set_entity_position(struct entity *entity, vec3 position);
void set_entity_scale(struct entity *entity, vec3 scale);
void set_entity_rotation(struct entity *entity, quat rotation);
vec3 local_entity_pos_to_world(struct entity *entity, vec3 position);

void init_car_entity(struct game_state *state, struct entity *car);
void update_car_entity(struct game_state *state, struct entity *car, float dt);
void draw_car_entity(struct game_state *state, struct entity *car);

void init_track_entity(struct game_state *state, struct entity *track,
        const char *track_type_name);
void update_track_entity(struct game_state *state, struct entity *track, float dt);
void draw_track_entity(struct game_state *state, struct entity *track);
void delete_track_entity(struct game_state *state, struct entity *track);
void set_track_entity_type(struct game_state *state, struct entity *track,
        const char *track_type_name);

void init_floor_entity(struct game_state *state, struct entity *floor);
void update_floor_entity(struct game_state *state, struct entity *floor, float dt);
void draw_floor_entity(struct game_state *state, struct entity *floor);

void init_checkpoint_entity(struct game_state *state, struct entity *checkpoint);
void update_checkpoint_entity(struct game_state *state, struct entity *checkpoint, float dt);
void draw_checkpoint_entity(struct game_state *state, struct entity *checkpoint);
void delete_checkpoint_entity(struct game_state *state, struct entity *checkpoint);

void init_extra_entity(struct game_state *state, struct entity *extra, const char *model_name);
void update_extra_entity(struct game_state *state, struct entity *extra, float dt);
void draw_extra_entity(struct game_state *state, struct entity *extra);
void delete_extra_entity(struct game_state *state, struct entity *extra);

#endif
