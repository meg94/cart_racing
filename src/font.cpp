#include "font.h"

#include <assert.h>

struct font load_font(const char *name, int tex_width) {
    struct font font;
    char temp_string[256];
    sprintf(temp_string, "assets/font/%s.sfl", name);

    FILE *f = fopen(temp_string, "r");

    int temp_int1, temp_int2;
    assert(fscanf(f, "%s\n", temp_string) == 1);
    assert(fscanf(f, "%d %d\n", &temp_int1, &temp_int2) == 2);
    assert(fscanf(f, "%s\n", temp_string) == 1);

    int num_chars;
    assert(fscanf(f, "%d\n", &num_chars) == 1);

    for (int i = 0; i < num_chars; i++) {
        int c, x, y, w, h, x_off, y_off, x_adv;
        assert(fscanf(f, "%d %d %d %d %d %d %d %d\n",
                    &c, &x, &y, &w, &h, &x_off, &y_off, &x_adv) == 8);

        font.chars[c].x = x;
        font.chars[c].y = y;
        font.chars[c].w = w;
        font.chars[c].h = h;
        font.chars[c].x_off = x_off;
        font.chars[c].y_off = y_off;
        font.chars[c].x_adv = x_adv;
    }

    fclose(f);

    sprintf(temp_string, "assets/font/%s.PNG", name);
    font.tex = load_texture(temp_string, GL_LINEAR);
    font.tex_width = tex_width;

    return font;
}
