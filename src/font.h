#ifndef _FONT_H
#define _FONT_H

#include "drawing.h"

struct font {
    int tex_width;
    GLuint tex;

    struct {
        int x, y,
            w, h,
            x_off, y_off,
            x_adv;
    } chars[256];
};

struct font load_font(const char *name, int tex_width);

#endif
