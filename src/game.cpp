#include <string>

#include "game.h"

void init_game_state(struct game_state *state, GLFWwindow *window) {
    for (int i = 0; i < GLFW_KEY_LAST; i++) {
        state->input.key_down[i] = false;
        state->input.key_clicked[i] = false;
    }
    state->input.left_mouse_down = false;
    state->input.left_mouse_clicked = false;

    state->entity_manager = new struct entity_manager();
    init_entity_manager(state->entity_manager);

    state->renderer_state = new struct renderer_state();
    init_renderer_state(state->renderer_state);

    state->physics_state = new struct physics_state();
    init_physics_state(state->physics_state);

    state->ingame_editor_state = new struct ingame_editor_state();
    init_ingame_editor_state(state->ingame_editor_state, window);

    {
        FILE *f = fopen("assets/model-list.txt", "r");
        assert(f);

        int i = 0;
        char name[MAX_TRACK_TYPE_NAME_LENGTH];
        char model_filename[2 * MAX_TRACK_TYPE_NAME_LENGTH];
        while (fscanf(f, "%s\n", name) != EOF) {
            sprintf(model_filename, "assets/models/%s.model_data", name);
            struct geometry_array *geometry_array = new struct geometry_array();
            struct material material;
            load_dae(model_filename, geometry_array, &material);
            struct collider_shape *collider_shape = create_geometry_collider_shape(geometry_array);

            state->renderer_state->geometry_arrays[std::string(name)] = geometry_array;
            state->renderer_state->materials[std::string(name)] = material;
            state->physics_state->collider_shapes[std::string(name)] = collider_shape;
            
            strcpy(state->model_names[i], name);
            i++;
        }
        state->num_model_names = i;

        fclose(f);
    }

    {
        FILE *f = fopen("assets/track-type-list.txt", "r");
        assert(f);

        int i = 0;
        char name[MAX_TRACK_TYPE_NAME_LENGTH];
        while (fscanf(f, "%s\n", name) != EOF) {
            strcpy(state->track_type_names[i], name);
            i++;
        }
        state->num_track_type_names = i;

        fclose(f);
    }

    {
        struct entity_id track_id = allocate_entity(state->entity_manager);
        struct entity *track = get_entity_from_id(state->entity_manager, track_id);
        init_track_entity(state, track, "roadCornerLarger");
        set_entity_position(track, V3(0.0f, 0.1f, 20.0f));
    }

    {
        struct entity_id track_id = allocate_entity(state->entity_manager);
        struct entity *track = get_entity_from_id(state->entity_manager, track_id);
        init_track_entity(state, track, "roadCornerLargerSand");
        set_entity_position(track, V3(0.0f, 0.1f, 20.0f));
    }

    {
        struct entity_id track_id = allocate_entity(state->entity_manager);
        struct entity *track = get_entity_from_id(state->entity_manager, track_id);
        init_track_entity(state, track, "roadCornerLargerWall");
        set_entity_position(track, V3(0.0f, 0.1f, 20.0f));
    }

    {
        struct entity_id track_id = allocate_entity(state->entity_manager);
        struct entity *track = get_entity_from_id(state->entity_manager, track_id);
        init_track_entity(state, track, "roadStraightLong");
        set_entity_position(track, V3(0.0f, 0.1f, 0.0f));
    }

    {
        struct entity_id track_id = allocate_entity(state->entity_manager);
        struct entity *track = get_entity_from_id(state->entity_manager, track_id);
        init_track_entity(state, track, "roadBump");
        set_entity_position(track, V3(10.0f, 0.1f, 0.0f));
        set_entity_rotation(track, quat_create_from_axis_angle(V3(0.0f, 1.0f, 0.0f), 1.0f * M_PI));
    }

    {
        struct entity_id car_id = allocate_entity(state->entity_manager);
        struct entity *car = get_entity_from_id(state->entity_manager, car_id);
        init_car_entity(state, car); 
        set_entity_position(car, V3(-5.0f, 1.0f, 0.0f));
        set_entity_rotation(car, quat_from_axis_angle(V3(0.0f, 1.0f, 0.0f), M_PI));

        state->follow_camera.entity_id = car_id;
        state->user_car_id = car_id;
    }

    {
        struct entity_id floor_id = allocate_entity(state->entity_manager);
        struct entity *floor = get_entity_from_id(state->entity_manager, floor_id);
        init_floor_entity(state, floor); 
    }

    state->camera_type = FOLLOW_CAMERA_TYPE;
    state->can_move_camera = false;
    state->can_move_car = true;
    state->free_camera.azimuth = 0.0f;
    state->free_camera.inclination = 0.5f * M_PI;
    state->free_camera.position = V3(0.0f, 10.0f, 0.0f);
    state->cur_checkpoint = 1;
    state->num_checkpoints = 0;
    state->is_first_lap = true;
    state->fastest_lap_time = 0.0f;
    state->current_lap_time = 0.0f;

    load_game_state_track(state, "assets/track.data");
}

void load_game_state_track(struct game_state *state, const char *filename) {
    struct entity_manager *entity_manager = state->entity_manager;
    for (int i = 0; i < entity_manager->cur_idx; i++) { 
        struct entity *entity = &entity_manager->entity_array[i];
        if (!entity_manager->active_array[i]) {
            continue;
        }

        switch (entity->type) {
            case ENTITY_TYPE_TRACK:
                delete_track_entity(state, entity);
                break;
            case ENTITY_TYPE_CHECKPOINT:
                delete_checkpoint_entity(state, entity);
                break;
            case ENTITY_TYPE_EXTRA:
                delete_extra_entity(state, entity);
                break;
            default:
                continue;
        }
    }

    state->cur_checkpoint = 1;
    state->num_checkpoints = 0;

    FILE *f = fopen("assets/track.data", "r");
    if (!f) {
        return;
    }

    int n;
    int matched_count = fscanf(f, "%d\n", &n); 
    assert(matched_count == 1);

    for (int i = 0; i < n; i++) {
        char entity_type[1024];
        assert(fscanf(f, "%s", entity_type) == 1);

        if (strcmp(entity_type, "track") == 0) {
            char track_type_name_temp[1024]; 
            vec3 p;
            quat r;
            matched_count = fscanf(f, "%s %f %f %f %f %f %f %f\n", track_type_name_temp,
                    &p.x, &p.y, &p.z,
                    &r.x, &r.y, &r.z, &r.w);
            assert(matched_count == 8);

            const char *track_type_name = NULL;
            for (int j = 0; j < state->num_track_type_names; j++) {
                if (strcmp(track_type_name_temp, state->track_type_names[j]) == 0) {
                    track_type_name = state->track_type_names[j];
                }
            }
            assert(track_type_name != NULL);

            struct entity_id id = allocate_entity(state->entity_manager);
            struct entity *entity = get_entity_from_id(state->entity_manager, id);
            init_track_entity(state, entity, track_type_name);
            set_entity_position(entity, p);
            set_entity_rotation(entity, r);
        } else if (strcmp(entity_type, "checkpoint") == 0) {
            vec3 p;
            vec3 s;
            quat r;
            int n;
            matched_count = fscanf(f, "%d %f %f %f %f %f %f %f %f %f %f\n",
                    &n,
                    &p.x, &p.y, &p.z,
                    &r.x, &r.y, &r.z, &r.w,
                    &s.x, &s.y, &s.z);
            assert(matched_count == 11);

            struct entity_id id = allocate_entity(state->entity_manager);
            struct entity *entity = get_entity_from_id(state->entity_manager, id);
            init_checkpoint_entity(state, entity);
            set_entity_position(entity, p);
            set_entity_rotation(entity, r);
            set_entity_scale(entity, s);
            entity->checkpoint.num = n;
        } else if (strcmp(entity_type, "extra") == 0) {
            char model_name_temp[1024]; 
            vec3 p;
            vec3 s;
            quat r;
            matched_count = fscanf(f, "%s %f %f %f %f %f %f %f %f %f %f\n",
                    model_name_temp,
                    &p.x, &p.y, &p.z,
                    &r.x, &r.y, &r.z, &r.w,
                    &s.x, &s.y, &s.z);
            assert(matched_count == 11);

            const char *model_name = NULL;
            for (int j = 0; j < state->num_model_names; j++) {
                if (strcmp(model_name_temp, state->model_names[j]) == 0) {
                    model_name = state->model_names[j];
                }
            }
            assert(model_name != NULL);

            struct entity_id id = allocate_entity(state->entity_manager);
            struct entity *entity = get_entity_from_id(state->entity_manager, id);
            init_extra_entity(state, entity, model_name);
            set_entity_position(entity, p);
            set_entity_rotation(entity, r);
            set_entity_scale(entity, s);
        } else if (strcmp(entity_type, "car") == 0) {
            vec3 p;
            quat r;
            matched_count = fscanf(f, "%f %f %f %f %f %f %f\n",
                    &p.x, &p.y, &p.z,
                    &r.x, &r.y, &r.z, &r.w);
            assert(matched_count == 7);

            struct entity_id car_id =  state->user_car_id;
            struct entity *car = get_entity_from_id(state->entity_manager, car_id);
            set_entity_position(car, p);
            set_entity_rotation(car, r);
        } else {
            assert(false);
        }
    }

    fclose(f);
}

void save_game_state_track(struct game_state *state, const char *filename) {
    int n = 0;
    struct entity *entities[MAX_NUM_ENTITIES];

    struct entity_manager *entity_manager = state->entity_manager;
    for (int i = 0; i < entity_manager->cur_idx; i++) { 
        struct entity *entity = &entity_manager->entity_array[i];
        if (!entity_manager->active_array[i]) {
            continue;
        }

        switch (entity->type) {
            case ENTITY_TYPE_TRACK:
            case ENTITY_TYPE_CHECKPOINT:
            case ENTITY_TYPE_EXTRA:
            case ENTITY_TYPE_CAR:
                break;
            default:
                continue;
        }
        entities[n] = entity;
        n++;
    }

    FILE *f = fopen("assets/track.data", "w");

    fprintf(f, "%d\n", n);
    for (int i = 0; i < n; i++) {
        struct entity *entity = entities[i];
        switch (entity->type) {
            case ENTITY_TYPE_CAR:
                {
                    vec3 p = entity->position;
                    quat r = entity->rotation;
                    fprintf(f, "car %f %f %f %f %f %f %f\n",
                            p.x, p.y, p.z, r.x, r.y, r.z, r.w);
                }
                break;
            case ENTITY_TYPE_TRACK:
                {
                    const char *name = entity->track.type_name;
                    vec3 p = entity->position;
                    quat r = entity->rotation;
                    fprintf(f, "track %s %f %f %f %f %f %f %f\n",
                            name, p.x, p.y, p.z, r.x, r.y, r.z, r.w);
                }
                break;
            case ENTITY_TYPE_CHECKPOINT: 
                {
                    vec3 p = entity->position;
                    vec3 s = entity->scale;
                    quat r = entity->rotation;
                    int n = entity->checkpoint.num;
                    fprintf(f, "checkpoint %d %f %f %f %f %f %f %f %f %f %f\n",
                            n, p.x, p.y, p.z, r.x, r.y, r.z, r.w, s.x, s.y, s.z);
                }
                break;
            case ENTITY_TYPE_EXTRA:
                {
                    vec3 p = entity->position;
                    vec3 s = entity->scale;
                    quat r = entity->rotation;
                    fprintf(f, "extra %s %f %f %f %f %f %f %f %f %f %f\n",
                            entity->extra.model_name,
                            p.x, p.y, p.z, r.x, r.y, r.z, r.w, s.x, s.y, s.z);
                }
                break;
            default:
                assert(false);
        }
    }

    fclose(f);
}
