#ifndef _GAME_H
#define _GAME_H

#define MAX_TRACK_TYPES 100
#define MAX_TRACK_TYPE_NAME_LENGTH 256
#define MAX_NUM_MODEL_NAMES 100
#define MAX_MODEL_NAME_LENGTH 256

#include "physics.h"
#include "drawing.h"
#include "ingame_editor.h"
#include "entity.h"
#include "GL/GL.h"

struct input {
    vec2 mouse_pos, mouse_pos_normalized;
    bool left_mouse_down, left_mouse_clicked;

    bool key_down[GLFW_KEY_LAST];
    bool key_clicked[GLFW_KEY_LAST];
};

enum camera_type {
    FOLLOW_CAMERA_TYPE, 
    FREE_CAMERA_TYPE
};

struct game_state {
    struct input input;
    struct entity_manager *entity_manager;
    struct physics_state *physics_state;
    struct renderer_state *renderer_state;
    struct ingame_editor_state *ingame_editor_state;
        
    int num_model_names;
    char model_names[MAX_MODEL_NAME_LENGTH][MAX_NUM_MODEL_NAMES];
    
    int num_track_type_names;
    char track_type_names[MAX_TRACK_TYPE_NAME_LENGTH][MAX_TRACK_TYPES];

    bool can_move_camera; 
    bool can_move_car;

    enum camera_type camera_type;

    struct {
        struct entity_id entity_id;
    } follow_camera;

    struct {
        float azimuth, inclination;
        vec3 position;
        vec3 direction;
    } free_camera;

    struct entity_id user_car_id;
    int cur_checkpoint;
    int num_checkpoints;

    struct geometry_array fastest_lap_text, current_lap_text;
    bool is_first_lap;
    float fastest_lap_time, current_lap_time;
};

void init_game_state(struct game_state *state, GLFWwindow *window);
void load_game_state_track(struct game_state *state, const char *filename);
void save_game_state_track(struct game_state *state, const char *filename);

#endif
