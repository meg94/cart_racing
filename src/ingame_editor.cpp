#include <stdio.h>
#include <map>

#include "ingame_editor.h"

/*
static void create_shadow_map(struct renderer_state *renderer, vec3 p[8]) {
    mat4 inv_shadow_world_mat = mat4_inverse(renderer->shadow_world_mat);

    vec3 bb0 = V3(FLT_MAX, FLT_MAX, FLT_MAX), 
         bb1 = V3(FLT_MIN, FLT_MIN, FLT_MIN);
    for (int i = 0; i < 8; i++) {
        p[i] = vec3_apply_mat4(p[i], 1.0f, inv_shadow_world_mat);

        if (p[i].x < bb0.x) {
            bb0.x = p[i].x;
        } 
        if (p[i].x > bb1.x) {
            bb1.x = p[i].x;
        }

        if (p[i].y < bb0.y) {
            bb0.y = p[i].y;
        } 
        if (p[i].y > bb1.y) {
            bb1.y = p[i].y;
        }

        if (p[i].z < bb0.z) {
            bb0.z = p[i].z;
        } 
        if (p[i].z > bb1.z) {
            bb1.z = p[i].z;
        }
    }

    float dx = bb1.x - bb0.x;
    float dy = bb1.y - bb0.y;
    float dz = bb1.z - bb0.z;

    bb0 = vec3_apply_mat4(bb0, 1.0f, renderer->shadow_world_mat); 
    bb1 = vec3_apply_mat4(bb1, 1.0f, renderer->shadow_world_mat); 

    //push_debug_cube(renderer, V3(0.0f, 1.0f, 0.0f), bb0, 0.1f);
    //push_debug_cube(renderer, V3(0.0f, 1.0f, 0.0f), bb1, 0.1f);

    //renderer->shadow_position = vec3_scale(vec3_add(bb0, bb1), 0.5f);
    //renderer->shadow_x = dx;
    //renderer->shadow_y = dy;
    //renderer->shadow_z = dz;
    //renderer->shadow_x = 15.0f;
    //renderer->shadow_y = 15.0f;
    //renderer->shadow_z = 5.0f;
}
*/

void init_ingame_editor_state(struct ingame_editor_state *state, GLFWwindow *window) {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 300 es");

    ImGui::StyleColorsDark();

    state->is_enabled = false;
    state->is_hovered = false;
    state->is_moving_entity = false;
    for (int i = 0; i < MAX_NUM_SELECTED_ENTITIES; i++) {
        state->selected_entities[i].idx = 0;
    }

    {
        state->car_editor.draw_suspension_info = false;
        state->car_editor.draw_collider = false;
        state->car_editor.car_id.idx = 0;
    }
}

static void draw_car_compression(struct renderer_state *renderer, struct entity *car, int i) {
    vec3 game_pos = local_entity_pos_to_world(car, car->car.wheel[i].local_position);
    vec4 game_pos_4 = V4(game_pos.x, game_pos.y, game_pos.z, 1.0f);
    vec4 screen_pos = vec4_apply_mat(game_pos_4, 
            mat4_multiply(renderer->proj_mat, renderer->view_mat));

    screen_pos.x = 1920.0f * 0.5f * ((screen_pos.x / screen_pos.w) + 1.0f); 
    screen_pos.y = 1080.0f * (1.0f - 0.5f * ((screen_pos.y / screen_pos.w) + 1.0f));

    char text[64];
    sprintf(text, "%0.2f", car->car.wheel[i].hit_fraction);

    ImGui::GetWindowDrawList()->AddText(
            ImVec2(screen_pos.x, screen_pos.y), 
            ImColor(1.0f,1.0f,1.0f,1.0f), 
            text);
}

static void draw_car_suspension_info(struct game_state *state, struct entity *car) {
    struct renderer_state *renderer = state->renderer_state;

    ImGui::SetNextWindowPos(ImVec2(0,0));
    ImGui::Begin("BCKGND", NULL, ImGui::GetIO().DisplaySize, 0.0f,
            ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
            ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar |
            ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoCollapse |
            ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoInputs |
            ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoBringToFrontOnFocus);

    draw_car_compression(renderer, car, 0);
    draw_car_compression(renderer, car, 1);
    draw_car_compression(renderer, car, 2);
    draw_car_compression(renderer, car, 3);

    ImGui::End();
}

static void select_entity(struct game_state *state, struct entity_id id) {
    struct ingame_editor_state *editor = state->ingame_editor_state;

    if (!state->input.key_down[GLFW_KEY_LEFT_SHIFT]) {
        for (int i = 0; i < MAX_NUM_SELECTED_ENTITIES; i++) {
            editor->selected_entities[i].idx = 0; 
        }
        editor->selected_entities[0] = id;
    } else {
        for (int i = 0; i < MAX_NUM_SELECTED_ENTITIES; i++) {
            if (get_entity_from_id(state->entity_manager, editor->selected_entities[i]) == NULL) {
                editor->selected_entities[i] = id;
                break;
            }
        }
    }
}

void draw_hovered_entity(struct game_state *state) {
    float x = state->input.mouse_pos_normalized.x;
    float y = state->input.mouse_pos_normalized.y;

    mat4 inv_proj = mat4_inverse(state->renderer_state->proj_mat);
    mat4 inv_view = mat4_inverse(state->renderer_state->view_mat);

    vec4 clip_space = {x, y, -1.0f, 1.0f};
    vec4 eye_space = vec4_apply_mat(clip_space, inv_proj);
    eye_space = V4(eye_space.x, eye_space.y, -1.0f, 0.0f);

    vec4 world_space_4 = vec4_apply_mat(eye_space, inv_view);
    vec3 world_space = V3(world_space_4.x, world_space_4.y, world_space_4.z);
    world_space = vec3_normalize(world_space);

    vec3 origin = state->free_camera.position;
    vec3 direction = world_space;
    state->ingame_editor_state->mouse_ray_direction = direction;
    float t;
    struct entity_id id;

    if (state->ingame_editor_state->is_moving_entity 
            || !ray_test(state->physics_state, origin, direction, &id, &t) 
            || state->ingame_editor_state->is_hovered) {
        return;
    }

    struct entity *entity = get_entity_from_id(state->entity_manager, id);
    if (!entity || 
            !(entity->type == ENTITY_TYPE_TRACK || 
                entity->type == ENTITY_TYPE_CHECKPOINT ||
                entity->type == ENTITY_TYPE_EXTRA)) {
        return;
    }

    if (state->input.left_mouse_clicked) {
        select_entity(state, id);
    }

    struct renderer_state *renderer = state->renderer_state;
    struct drawable drawable = create_drawable();
    switch (entity->type) {
        case ENTITY_TYPE_TRACK:
            drawable.geometry_array = get_geometry_array(renderer, entity->track.type_name);
            break;
        case ENTITY_TYPE_CHECKPOINT:
            drawable.geometry_array = get_geometry_array(renderer, "cube");
            break;
        case ENTITY_TYPE_EXTRA:
            drawable.geometry_array = get_geometry_array(renderer, entity->extra.model_name);
            break;
        default:
            assert(false);
    }
    drawable.material = wireframe_material(V3(1.0f, 0.0f, 0.0f));
    drawable.casts_shadow = false;
    drawable.model_mat = mat4_multiply_n(3, 
            mat4_translation(entity->position),
            mat4_from_quat(entity->rotation),
            mat4_scale(entity->scale));
    for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
        drawable.pre_transformation[i] = mat4_identity();
    }
    push_drawable(renderer, drawable);
}

static void draw_selected_entity(struct game_state *state) {
    struct renderer_state *renderer = state->renderer_state;
    struct ingame_editor_state *editor = state->ingame_editor_state;

    for (int i = 0; i < MAX_NUM_SELECTED_ENTITIES; i++) {
        struct entity_id id = editor->selected_entities[i];
        struct entity *entity = get_entity_from_id(state->entity_manager, id);
        if (!entity) {
            continue;
        }

        struct drawable drawable = create_drawable();
        switch (entity->type) {
            case ENTITY_TYPE_TRACK:
                drawable.geometry_array = get_geometry_array(renderer, entity->track.type_name);
                break;
            case ENTITY_TYPE_CHECKPOINT:
                drawable.geometry_array = get_geometry_array(renderer, "cube");
                break;
            case ENTITY_TYPE_EXTRA:
                drawable.geometry_array = get_geometry_array(renderer, entity->extra.model_name);
                break;
            default:
                assert(false);
        }
        drawable.material = wireframe_material(V3(0.0f, 1.0f, 0.0f));
        drawable.casts_shadow = false;
        drawable.model_mat = mat4_multiply_n(3, 
                mat4_translation(entity->position),
                mat4_from_quat(entity->rotation),
                mat4_scale(entity->scale));
        for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
            drawable.pre_transformation[i] = mat4_identity();
        }
        push_drawable(renderer, drawable);
    }
}

static vec3 convert_to_track_position(vec3 position) {
    position.x = position.x - fmodf(position.x, 10.0f);
    position.z = position.z - fmodf(position.z, 10.0f);
    return position;
}

static void draw_track_entity_editor(struct game_state *state, struct entity *track) {
    //
    // Rotation
    //

    int rotation_idx = 0;
    {
        mat4 basis = mat4_from_quat(track->rotation);
        vec3 x = V3(basis.m[0], basis.m[4], basis.m[8]);

        float largest_dp = -1.0f;
        float dp0 = vec3_dot(x, V3(1.0f, 0.0f, 0.0f));
        float dp1 = vec3_dot(x, V3(0.0f, 0.0, -1.0f));
        float dp2 = vec3_dot(x, V3(-1.0f, 0.0, 0.0f));
        float dp3 = vec3_dot(x, V3(0.0f, 0.0, 1.0f));

        if (dp0 > largest_dp) {
            largest_dp = dp0;
            rotation_idx = 0;
        }
        if (dp1 > largest_dp) {
            largest_dp = dp1;
            rotation_idx = 1;
        }
        if (dp2 > largest_dp) {
            largest_dp = dp2;
            rotation_idx = 2;
        }
        if (dp3 > largest_dp) {
            largest_dp = dp3;
            rotation_idx = 3;
        }
    }

    ImGui::SliderInt("Rotation", &rotation_idx, 0, 3);

    if (rotation_idx == 0) {
        quat rotation = quat_create_from_axis_angle(V3(0.0f, 1.0f, 0.0f), 0.0f);
        set_entity_rotation(track, rotation);
    } else if (rotation_idx == 1) {
        quat rotation = quat_create_from_axis_angle(V3(0.0f, 1.0f, 0.0f), 0.5f * M_PI);
        set_entity_rotation(track, rotation);
    } else if (rotation_idx == 2) {
        quat rotation = quat_create_from_axis_angle(V3(0.0f, 1.0f, 0.0f), 1.0f * M_PI);
        set_entity_rotation(track, rotation);
    } else if (rotation_idx == 3) {
        quat rotation = quat_create_from_axis_angle(V3(0.0f, 1.0f, 0.0f), 1.5f * M_PI);
        set_entity_rotation(track, rotation);
    }

    //
    // Track Type
    //

    int current_item = 0;
    for (int i = 0; i < state->num_track_type_names; i++) {
        const char *name = state->track_type_names[i]; 
        if (strcmp(name, track->track.type_name) == 0) {
            current_item = i;
        }
    }

    int prev_current_item = current_item;
    char *track_type_names[MAX_TRACK_TYPES];
    for (int i = 0; i < state->num_track_type_names; i++) {
        track_type_names[i] = state->track_type_names[i];
    }
    ImGui::ListBox("Track Type", &current_item, track_type_names, 
            state->num_track_type_names);
    if (prev_current_item != current_item) {
        set_track_entity_type(state, track, 
                state->track_type_names[current_item]); 
    } 

    //
    // Move
    //

    if (state->ingame_editor_state->is_moving_entity) {
        vec3 ro = state->free_camera.position;  
        vec3 rd = state->ingame_editor_state->mouse_ray_direction;
        float t = (0.1f - ro.y) / rd.y;
        vec3 p = vec3_add(ro, vec3_scale(rd, t));
        p = convert_to_track_position(p);
        set_entity_position(track, p);

        if (state->input.left_mouse_clicked && !state->ingame_editor_state->is_hovered) {
            state->ingame_editor_state->is_moving_entity = false;
        }
    }

    if (ImGui::Button("Move")) {
        state->ingame_editor_state->is_moving_entity = true;
    }
}

static void draw_checkpoint_entity_editor(struct game_state *state, struct entity *checkpoint) {
    //
    // Move
    //
    if (state->ingame_editor_state->is_moving_entity) {
        vec3 ro = state->free_camera.position;  
        vec3 rd = state->ingame_editor_state->mouse_ray_direction;
        float t = (checkpoint->position.y - ro.y) / rd.y;
        vec3 p = vec3_add(ro, vec3_scale(rd, t));
        set_entity_position(checkpoint, p);

        if (state->input.left_mouse_clicked && !state->ingame_editor_state->is_hovered) {
            state->ingame_editor_state->is_moving_entity = false;
        }
    }

    if (ImGui::Button("Move")) {
        state->ingame_editor_state->is_moving_entity = true;
    }

    //
    // Scale
    //
    vec3 new_scale = checkpoint->scale;
    if (ImGui::InputFloat3("Scale", (float*) &new_scale)) {
        set_entity_scale(checkpoint, new_scale);
    }

    //
    // Y-Rotation
    //
    float y_rotation; 
    {
        mat4 basis = mat4_from_quat(checkpoint->rotation); 
        vec3 x = V3(basis.m[0], basis.m[4], basis.m[8]); 
        float dot = vec3_dot(x, V3(1.0f, 0.0f, 0.0f));
        y_rotation = acosf(dot);
        if (x.z > 0.0f) {
            y_rotation *= -1.0f;
        }
    }
    ImGui::InputFloat("Y-Rotation", &y_rotation);
    set_entity_rotation(checkpoint, quat_from_axis_angle(V3(0.0f, 1.0f, 0.0f), y_rotation));

    //
    // Y-Pos
    //
    ImGui::InputFloat("Y-Pos", &checkpoint->position.y);
    
    //
    // Checkpoint num
    //
    ImGui::InputInt("Checkpoint Num", &checkpoint->checkpoint.num);
    ImGui::SameLine();
    ImGui::Text(" / %d", state->num_checkpoints);
}

static void draw_extra_entity_editor(struct game_state *state, struct entity *extra) {
    //
    // Move
    //
    if (state->ingame_editor_state->is_moving_entity) {
        vec3 ro = state->free_camera.position;  
        vec3 rd = state->ingame_editor_state->mouse_ray_direction;
        float t = (extra->position.y - ro.y) / rd.y;
        vec3 p = vec3_add(ro, vec3_scale(rd, t));
        set_entity_position(extra, p);

        if (state->input.left_mouse_clicked && !state->ingame_editor_state->is_hovered) {
            state->ingame_editor_state->is_moving_entity = false;
        }
    }

    if (ImGui::Button("Move")) {
        state->ingame_editor_state->is_moving_entity = true;
    }

    //
    // Y-Rotation
    //
    float y_rotation; 
    {
        mat4 basis = mat4_from_quat(extra->rotation); 
        vec3 x = V3(basis.m[0], basis.m[4], basis.m[8]); 
        float dot = vec3_dot(x, V3(1.0f, 0.0f, 0.0f));
        y_rotation = acosf(dot);
    } 
    ImGui::InputFloat("Y-Rotation", &y_rotation, 0.0f, M_PI);
    set_entity_rotation(extra, quat_from_axis_angle(V3(0.0f, 1.0f, 0.0f), y_rotation));

    //
    // Y-Pos
    //
    ImGui::InputFloat("Y-Pos", &extra->position.y);
}

static void draw_track_editor(struct game_state *state) {
    struct ingame_editor_state *editor = state->ingame_editor_state;

    ImGui::Begin("Track Editor");
    if (ImGui::Button("Save")) {
        save_game_state_track(state, "assets/track.data"); 
    }
    if (ImGui::Button("Load")) {
        load_game_state_track(state, "assets/track.data"); 
    }
    if (ImGui::Button("Add Track Entity")) {
        vec3 ro = state->free_camera.position; 
        vec3 rd = state->free_camera.direction;
        float t = (0.1f - ro.y) / rd.y;
        vec3 p = vec3_add(ro, vec3_scale(rd, t));
        p = convert_to_track_position(p);
        struct entity_id track_id = allocate_entity(state->entity_manager); 
        struct entity *track = get_entity_from_id(state->entity_manager, track_id);
        init_track_entity(state, track, state->track_type_names[0]);
        set_entity_position(track, p);
    }
    if (ImGui::Button("Add Checkpoint Entity")) {
        vec3 ro = state->free_camera.position; 
        vec3 rd = state->free_camera.direction;
        float t = (0.1f - ro.y) / rd.y;
        vec3 p = vec3_add(ro, vec3_scale(rd, t));
        struct entity_id checkpoint_id = allocate_entity(state->entity_manager); 
        struct entity *checkpoint = get_entity_from_id(state->entity_manager, checkpoint_id);
        init_checkpoint_entity(state, checkpoint);
        set_entity_position(checkpoint, p);
    }
    if (ImGui::Button("Add Extra Entity")) {
        vec3 ro = state->free_camera.position; 
        vec3 rd = state->free_camera.direction;
        float t = (0.1f - ro.y) / rd.y;
        vec3 p = vec3_add(ro, vec3_scale(rd, t));
        struct entity_id extra_id = allocate_entity(state->entity_manager); 
        struct entity *extra = get_entity_from_id(state->entity_manager, extra_id);
        init_extra_entity(state, extra, "overhead");
        set_entity_position(extra, p);
    }

    int num_selected_entities = 0;
    struct entity *first_selected_entity = NULL;
    for (int i = 0; i < MAX_NUM_SELECTED_ENTITIES; i++) {
        struct entity_id id = editor->selected_entities[i];
        struct entity *entity = get_entity_from_id(state->entity_manager, id);
        if (!entity) {
            continue;
        }
        num_selected_entities++;
        if (!first_selected_entity) {
            first_selected_entity = entity;
        }
    }

    if (num_selected_entities == 1) {
        switch (first_selected_entity->type) {
            case ENTITY_TYPE_TRACK:
                draw_track_entity_editor(state, first_selected_entity);
                break;
            case ENTITY_TYPE_CHECKPOINT:
                draw_checkpoint_entity_editor(state, first_selected_entity);
                break;
            case ENTITY_TYPE_EXTRA:
                draw_extra_entity_editor(state, first_selected_entity);
                break;
            default:
                assert(false);
        }

    } else if (num_selected_entities > 1) {
        //
        // Move
        //
        if (state->ingame_editor_state->is_moving_entity) {
            vec3 ro = state->free_camera.position;  
            vec3 rd = state->ingame_editor_state->mouse_ray_direction;
            float t = (0.1f - ro.y) / rd.y;
            vec3 p = vec3_add(ro, vec3_scale(rd, t));
            p = convert_to_track_position(p);
            //set_entity_position(first_selected_entity, p);
            vec3 first_entity_p = first_selected_entity->position;

            for (int i = 0; i < MAX_NUM_SELECTED_ENTITIES; i++) {
                struct entity_id id = editor->selected_entities[i];
                struct entity *entity = get_entity_from_id(state->entity_manager, id);
                if (!entity) {
                    continue;
                }
                vec3 entity_p = vec3_subtract(entity->position, first_entity_p);
                entity_p = vec3_add(entity_p, p);
                set_entity_position(entity, entity_p);
            }

            if (state->input.left_mouse_clicked && !state->ingame_editor_state->is_hovered) {
                state->ingame_editor_state->is_moving_entity = false;
            }
        }

        if (ImGui::Button("Move")) {
            state->ingame_editor_state->is_moving_entity = true;
        }
    }

    ImGui::End();
}

static void draw_shadow_editor(struct game_state *state) {
    struct renderer_state *renderer = state->renderer_state;

    ImGui::Begin("Shadow Editor");
    //ImGui::Image((void*)(intptr_t)state->renderer_state->shadow_tex, ImVec2(256, 256));
    ImGui::InputFloat3("Position", (float*) (&renderer->shadow_position));
    ImGui::InputFloat3("Direction", (float*) (&renderer->shadow_direction));
    ImGui::InputFloat3("Up", (float*) (&renderer->shadow_up));
    ImGui::InputFloat("x", &renderer->shadow_x);
    ImGui::InputFloat("y", &renderer->shadow_y);
    ImGui::InputFloat("z", &renderer->shadow_z);

    ImGui::End();

    {
        vec3 f = vec3_normalize(renderer->shadow_direction);
        vec3 s = vec3_normalize(vec3_cross(f, renderer->shadow_up));
        vec3 u = vec3_normalize(vec3_cross(s, f));

        mat4 scale = mat4_scale(V3(
                    renderer->shadow_x,
                    renderer->shadow_y,
                    renderer->shadow_z));
        mat4 rotation = mat4_create(
                s.x, u.x, f.x, 0.0f,
                s.y, u.y, f.y, 0.0f,
                s.z, u.z, f.z, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
        mat4 translation = mat4_translation(renderer->shadow_position);
        mat4 model_mat = mat4_multiply_n(3, translation, rotation, scale);

        struct drawable drawable = create_drawable();
        drawable.geometry_array = get_geometry_array(renderer, "cube");
        drawable.material = wireframe_material(V3(1.0f, 1.0f, 1.0f));
        //drawable.material = color_material(V3(1.0f, 0.0f, 0.0f));
        drawable.casts_shadow = false;
        drawable.model_mat = model_mat;
        for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
            drawable.pre_transformation[i] = mat4_identity();
        }
        //push_drawable(renderer, drawable);
    }

    /*
    {
        vec3 corners[8];
        vec4 corners4[8];
        corners4[0] = V4(-1.0f, -1.0f, 1.0f, 1.0f); 
        corners4[1] = V4(1.0f, -1.0f, 1.0f, 1.0f); 
        corners4[2] = V4(-1.0f, 1.0f, 1.0f, 1.0f); 
        corners4[3] = V4(1.0f, 1.0f, 1.0f, 1.0f); 
        corners4[4] = V4(-1.0f, -1.0f, -1.0f, 1.0f); 
        corners4[5] = V4(1.0f, -1.0f, -1.0f, 1.0f); 
        corners4[6] = V4(-1.0f, 1.0f, -1.0f, 1.0f); 
        corners4[7] = V4(1.0f, 1.0f, -1.0f, 1.0f); 

        for (int i = 0; i < 8; i++) {
            corners4[i] = vec4_apply_mat(corners4[i], mat4_inverse(renderer->car_proj_mat));
            corners4[i] = vec4_apply_mat(corners4[i], mat4_inverse(renderer->car_view_mat));
            corners4[i] = vec4_scale(corners4[i], 1.0f / corners4[i].w);
            corners[i] = V3(corners4[i].x, corners4[i].y, corners4[i].z);
        }

        push_debug_cube(renderer, V3(1.0f, 0.0f, 0.0f), corners[0], 0.2f);
        push_debug_cube(renderer, V3(0.0f, 1.0f, 0.0f), corners[1], 0.2f);
        push_debug_cube(renderer, V3(0.0f, 0.0f, 1.0f), corners[2], 0.2f);
        push_debug_cube(renderer, V3(1.0f, 1.0f, 0.0f), corners[3], 0.2f);
        push_debug_cube(renderer, V3(1.0f, 0.0f, 0.0f), corners[4], 0.2f);
        push_debug_cube(renderer, V3(0.0f, 1.0f, 0.0f), corners[5], 0.2f);
        push_debug_cube(renderer, V3(0.0f, 0.0f, 1.0f), corners[6], 0.2f);
        push_debug_cube(renderer, V3(1.0f, 1.0f, 0.0f), corners[7], 0.2f);

        push_debug_line_drawable(renderer, corners[4], corners[5]);
        push_debug_line_drawable(renderer, corners[4], corners[6]);
        push_debug_line_drawable(renderer, corners[5], corners[7]);
        push_debug_line_drawable(renderer, corners[6], corners[7]);

        vec3 p[8];
        for (int i = 0; i < 6; i++) {
            p[i] = corners[2 + i];
        }
        p[6] = V3(p[0].x, 0.0f, p[0].z);
        p[7] = V3(p[1].x, 0.0f, p[1].z);
        //create_shadow_map(renderer, p);
    } 
    */
}

static void draw_general_editor(struct game_state *state) {
    ImGui::Begin("General Editor");

    ImGui::RadioButton("Follow Camera", (int*) &state->camera_type, FOLLOW_CAMERA_TYPE); 
    ImGui::SameLine();
    ImGui::RadioButton("Free Camera", (int*) &state->camera_type, FREE_CAMERA_TYPE); 

    ImGui::Checkbox("Move Camera", &state->can_move_camera);
    ImGui::SameLine();
    ImGui::Checkbox("Move Car", &state->can_move_car);

    ImGui::End();
}

static void draw_car_editor(struct game_state *state) {
    struct ingame_editor_state *ingame_editor = state->ingame_editor_state;
    struct renderer_state *renderer = state->renderer_state;

    struct entity_id car_id = ingame_editor->car_editor.car_id;
    struct entity *car = get_entity_from_id(state->entity_manager, car_id);

    ImGui::Begin("Car Editor");
    ImGui::Checkbox("Draw Suspension Info", &ingame_editor->car_editor.draw_suspension_info);
    ImGui::Checkbox("Draw Collider", &ingame_editor->car_editor.draw_collider);
    ImGui::InputFloat3("Mesh DY", (float*) &car->car.mesh_pos);
    ImGui::End();

    struct entity_manager *entity_manager = state->entity_manager;
    for (int i = 0; i < entity_manager->cur_idx; i++) { 
        struct entity *entity = &entity_manager->entity_array[i];
        if (!entity_manager->active_array[i]) {
            continue;
        }
        if (entity->type == ENTITY_TYPE_CAR) {
            if (ingame_editor->car_editor.draw_suspension_info) {
                draw_car_suspension_info(state, entity);
            }

            if (ingame_editor->car_editor.draw_collider) {
                struct drawable drawable = create_drawable();
                drawable.geometry_array = get_geometry_array(renderer, "cube");
                drawable.material = color_material(V3(1.0f, 0.0f, 0.0f));
                drawable.casts_shadow = false;
                drawable.model_mat = mat4_multiply_n(3,
                        mat4_translation(entity->position),
                        mat4_from_quat(entity->rotation),
                        mat4_scale(V3(0.7f, 0.3f, 1.0f)));
                for (int i = 0; i < drawable.geometry_array->num_geometries; i++) {
                    drawable.pre_transformation[i] = mat4_identity();
                }
                push_drawable(renderer, drawable);
            }
        }
    }
}

void draw_ingame_editor(struct game_state *state) {
    if (state->input.key_clicked[GLFW_KEY_F12]) {
        state->ingame_editor_state->is_enabled = !state->ingame_editor_state->is_enabled;
    }

    if (!state->ingame_editor_state->is_enabled) {
        return;
    }

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    state->ingame_editor_state->is_hovered = ImGui::GetIO().WantCaptureMouse;
    state->ingame_editor_state->car_editor.car_id = state->follow_camera.entity_id;
    {
        struct entity_id id = state->ingame_editor_state->car_editor.car_id;
        struct entity *car = get_entity_from_id(state->entity_manager, id);
        assert(car && car->type == ENTITY_TYPE_CAR);
    }

    //draw_entity_editor(state);
    draw_selected_entity(state);
    draw_hovered_entity(state);
    draw_track_editor(state);
    draw_shadow_editor(state);
    draw_car_editor(state);
    draw_general_editor(state);

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
