#ifndef _INGAME_EDITOR
#define _INGAME_EDITOR

#include "GL/GL.h"
#include "game.h"
#include "entity.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#define MAX_NUM_SELECTED_ENTITIES 100

struct ingame_editor_state {
    bool is_enabled;
    bool is_hovered;

    bool is_moving_entity;
    vec3 mouse_ray_direction;

    struct entity_id selected_entities[MAX_NUM_SELECTED_ENTITIES];

    struct {
        int rotation_idx;
    } selected_entity_editor;

    struct {
        struct entity_id car_id;
        bool draw_suspension_info;
        bool draw_collider;
    } car_editor;
};

void init_ingame_editor_state(struct ingame_editor_state *state, GLFWwindow *window);
void draw_ingame_editor(struct game_state *state);

#endif
