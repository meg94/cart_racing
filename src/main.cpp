#include <stdio.h>
#include <string.h>

#if JS_BUILD
#include <emscripten/emscripten.h>
#endif

#include "drawing.h"
#include "entity.h"
#include "maths/maths.h"
#include "GL/GL.h"
#include "physics.h"
#include "drawing.h"
#include "ingame_editor.h"
#include "game.h"

static GLFWwindow *window;
static int refresh_rate;
static struct game_state *game_state;
static mat4 proj_mat, view_mat; 

static int create_window(const char *title, int width, int height, GLFWwindow **window, int *refresh_rate) {
    if (!glfwInit()) {
        return 1;
    }

    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    *refresh_rate = mode->refreshRate;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_SAMPLES, 4);
    *window = glfwCreateWindow(width, height, title, NULL, NULL);
    if (!*window) {
        return 1;
    }

    glfwMakeContextCurrent(*window);
    glViewport(0, 0, width, height);
    glfwSwapInterval(1);
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        return 1;
    }

    return 0;
}

static void check_if_clicked(int state, bool *clicked, bool *down) {
    *clicked = false;
    if (state == GLFW_PRESS && !(*down)) {
        *down = true;
    }
    if (state == GLFW_RELEASE && (*down)) {
        *clicked = true;
        *down = false;
    }
}

static void update_inputs(struct input *inputs) {
    for (int c = 0; c < GLFW_KEY_LAST; c++) {
        check_if_clicked(glfwGetKey(window, c),
                         &inputs->key_clicked[c],
                         &inputs->key_down[c]);
    }

    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        inputs->mouse_pos = V2((float) xpos, (float) ypos);

        xpos = xpos / 1920.0;
        ypos = 1.0 - ypos / 1080.0;
        xpos = -1.0 + 2.0 * xpos;
        ypos = -1.0 + 2.0 * ypos;
        inputs->mouse_pos_normalized = V2((float) xpos, (float) ypos);

        int mouse_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
        check_if_clicked(mouse_state, &inputs->left_mouse_clicked, &inputs->left_mouse_down);
    }
}

static void frame() {
    glfwPollEvents();
    update_inputs(&game_state->input);

    float dt = 1.0f / 60.0f;
    if (refresh_rate != 0) {
        dt = 1.0f / refresh_rate;
    }
    update_physics_state(game_state, 1.0f / refresh_rate);

    game_state->current_lap_time += dt;

    {
        struct entity *car = get_entity_from_id(game_state->entity_manager,
                game_state->follow_camera.entity_id);
        assert(car);

        car->car.is_user_controlled = game_state->can_move_car;

        vec3 cam_targ = car->position;
        vec3 cam_pos = V3(0.0f, 1.0f, 2.5f);
        cam_pos = local_entity_pos_to_world(car, cam_pos);

        game_state->renderer_state->car_view_mat = 
            mat4_look_at(cam_pos, cam_targ, V3(0.0f, 1.0f, 0.0f));
        game_state->renderer_state->car_proj_mat = 
            mat4_perspective_projection(66.0f, 1920.0f / 1080.0f, 1.0f, 200.0f); 

        cam_pos.x = cam_pos.x - fmodf(cam_pos.x, 1.0f / 2048.0f);
        cam_pos.y = cam_pos.y - fmodf(cam_pos.y, 1.0f / 2048.0f);
        cam_pos.z = cam_pos.z - fmodf(cam_pos.z, 1.0f / 2048.0f);

        game_state->renderer_state->shadow_position = cam_pos;
    }

    if (game_state->can_move_camera) {
        float theta = game_state->free_camera.inclination;
        float phi = game_state->free_camera.azimuth;
        float sin_theta = sinf(theta);
        float cos_theta = cosf(theta);
        float sin_phi = sinf(phi);
        float cos_phi = cosf(phi);
        vec3 cam_dir  = V3(sin_theta * cos_phi, cos_theta, sin_theta * sin_phi);
        game_state->free_camera.direction = cam_dir;
        vec3 cam_pos = game_state->free_camera.position;
        vec3 cam_target = vec3_add(cam_pos, cam_dir);
        view_mat = mat4_look_at(cam_pos, cam_target, V3(0.0f, 1.0f, 0.0f));

        float camera_rotate_delta = 1.0f * dt;
        if (game_state->input.key_down[GLFW_KEY_RIGHT]) {
            game_state->free_camera.azimuth += camera_rotate_delta; 
        }
        if (game_state->input.key_down[GLFW_KEY_LEFT]) {
            game_state->free_camera.azimuth -= camera_rotate_delta; 
        }
        if (game_state->input.key_down[GLFW_KEY_UP]) {
            game_state->free_camera.inclination -= camera_rotate_delta; 
        }
        if (game_state->input.key_down[GLFW_KEY_DOWN]) {
            game_state->free_camera.inclination += camera_rotate_delta; 
        }

        float camera_movement_delta = 10.0f * dt;
        float dx = camera_movement_delta * cos_phi;
        float dz = camera_movement_delta * sin_phi;
        if (game_state->input.key_down[GLFW_KEY_W]) {
            game_state->free_camera.position.x += dx;
            game_state->free_camera.position.z += dz;
        }
        if (game_state->input.key_down[GLFW_KEY_S]) {
            game_state->free_camera.position.x -= dx;
            game_state->free_camera.position.z -= dz;
        }
        if (game_state->input.key_down[GLFW_KEY_A]) {
            game_state->free_camera.position.x += dz;
            game_state->free_camera.position.z -= dx;
        }
        if (game_state->input.key_down[GLFW_KEY_D]) {
            game_state->free_camera.position.x -= dz;
            game_state->free_camera.position.z += dx;
        }
        if (game_state->input.key_down[GLFW_KEY_Q]) {
            game_state->free_camera.position.y += camera_movement_delta;
        }
        if (game_state->input.key_down[GLFW_KEY_E]) {
            game_state->free_camera.position.y -= camera_movement_delta;
        }
    }

    struct entity_manager *entity_manager = game_state->entity_manager;
    for (int i = 0; i < entity_manager->cur_idx; i++) {
        if (!entity_manager->active_array[i]) {
            continue;
        }
        struct entity *entity = &entity_manager->entity_array[i];

        switch (entity->type) {
            case ENTITY_TYPE_TRACK:
                update_track_entity(game_state, entity, 1.0f / refresh_rate);
                draw_track_entity(game_state, entity);
                break;
            case ENTITY_TYPE_CAR:
                update_car_entity(game_state, entity, 1.0f / refresh_rate);
                draw_car_entity(game_state, entity);
                break;
            case ENTITY_TYPE_FLOOR:
                update_floor_entity(game_state, entity, 1.0f / refresh_rate);
                draw_floor_entity(game_state, entity);
                break;
            case ENTITY_TYPE_CHECKPOINT:
                update_checkpoint_entity(game_state, entity, 1.0f / refresh_rate); 
                draw_checkpoint_entity(game_state, entity);
                break;
            case ENTITY_TYPE_EXTRA:
                update_extra_entity(game_state, entity, 1.0f / refresh_rate); 
                draw_extra_entity(game_state, entity);
                break;
            case ENTITY_NUM_TYPES:
                break;
            default:
                assert(false);
                break;
        }
    }

    {
        char text[256];
        sprintf(text, "Current lap: %.2f", game_state->current_lap_time);

        struct font consolas = game_state->renderer_state->fonts["consolas_bold_48"];
        update_text_geometry_array(game_state->renderer_state, consolas, 
                &game_state->current_lap_text, text);
        struct drawable drawable = create_drawable();
        drawable.geometry_array = &game_state->current_lap_text;
        drawable.material = colored_image_material(V3(1.0f, 1.0f, 1.0f),
                game_state->renderer_state->fonts["consolas_bold_48"].tex);
        drawable.model_mat = mat4_translation(V3(20.0f, 1270.0f, 0.0f));
        drawable.pre_transformation[0] = mat4_identity();
        push_ui_drawable(game_state->renderer_state, drawable);
    }

    {
        char text[256];
        if (game_state->is_first_lap) {
            sprintf(text, "Fastest lap:");
        } else {
            sprintf(text, "Fastest lap: %.2f", game_state->fastest_lap_time);
        }

        struct font consolas = game_state->renderer_state->fonts["consolas_bold_48"];
        update_text_geometry_array(game_state->renderer_state, consolas, 
                &game_state->fastest_lap_text, text);
        struct drawable drawable = create_drawable();
        drawable.geometry_array = &game_state->fastest_lap_text;
        drawable.material = colored_image_material(V3(1.0f, 1.0f, 1.0f),
                game_state->renderer_state->fonts["consolas_bold_48"].tex);
        drawable.model_mat = mat4_translation(V3(20.0f, 1180.0f, 0.0f));
        drawable.pre_transformation[0] = mat4_identity();
        push_ui_drawable(game_state->renderer_state, drawable);
    }

    game_state->renderer_state->view_mat = view_mat;
    game_state->renderer_state->proj_mat = proj_mat;

    if (game_state->camera_type == FOLLOW_CAMERA_TYPE) {
        game_state->renderer_state->view_mat = game_state->renderer_state->car_view_mat;
        game_state->renderer_state->proj_mat = game_state->renderer_state->car_proj_mat;
    }

    draw_renderer_state(game_state->renderer_state);
    draw_ingame_editor(game_state);

    glfwSwapBuffers(window);
}

int main(int argc, char **args) {
    if (create_window("opengl", 1920, 1080, &window, &refresh_rate)) {
        printf("Could not create GLFW window");
        return 1;
    }

    proj_mat = mat4_perspective_projection(66.0f, 1920.0f / 1080.0f, 2.0f, 200.0f); 

    game_state = new struct game_state();
    init_game_state(game_state, window);

    struct font consolas = game_state->renderer_state->fonts["consolas_bold_48"];
    init_text_geometry_array(game_state->renderer_state, &game_state->fastest_lap_text);
    init_text_geometry_array(game_state->renderer_state, &game_state->current_lap_text);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if JS_BUILD
    emscripten_set_main_loop(frame, 0, 0);
#endif
#if LINUX_BUILD
    while (!glfwWindowShouldClose(window)) {
        frame();
    }
#endif

    return 0;
}
