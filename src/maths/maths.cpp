#include "maths.h"

float rand_num() {
    return rand() / ((float)RAND_MAX);
}

struct ray ray_create(vec3 orig, vec3 dir) {
    return (struct ray){
        .orig = orig,
        .dir = dir,
    };
}

struct ray_2D ray_2D_create(vec2 orig, vec2 dir) {
    return (struct ray_2D){
        .orig = orig,
        .dir = dir,
    };
}

vec2 ray_2D_at_time(struct ray_2D r, float t) {
    return vec2_add(r.orig, vec2_scale(r.dir, t));
}

struct bounding_box bounding_box_create(vec3 center, vec3 half_lengths) {
    return (struct bounding_box){
        .center = center,
        .half_lengths = half_lengths,
    };
}

bool ray_2D_intersect(struct ray_2D r1, struct ray_2D r2, float *t1, float *t2) {
    mat4 m = mat4_create(r1.dir.x, -r2.dir.x, 0.0f, 0.0f,
                         r1.dir.y, -r2.dir.y, 0.0f, 0.0f,
                         0.0f, 0.0f, 1.0f, 0.0f,
                         0.0f, 0.0f, 0.0f, 1.0f);
    mat4 mi = mat4_inverse(m);
    vec4 v = vec4_create(r2.orig.x - r1.orig.x, r2.orig.y - r1.orig.y, 0.0f, 0.0f);
    vec4 t_v = vec4_apply_mat(v, mi);
    *t1 = t_v.x;
    *t2 = t_v.y;
    return true;
}

bool ray_intersect_bounding_box(struct ray r, struct bounding_box bb, float *t) {
    vec3 min = vec3_subtract(bb.center, vec3_scale(bb.half_lengths, 0.5f));
    vec3 max = vec3_add(bb.center, vec3_scale(bb.half_lengths, 0.5f));

    float tmin = (min.x - r.orig.x) / r.dir.x;
    float tmax = (max.x - r.orig.x) / r.dir.x;

    if (tmin > tmax) {
        float temp = tmin;
        tmin = tmax;
        tmax = temp;
    }

    float tymin = (min.y - r.orig.y) / r.dir.y;
    float tymax = (max.y - r.orig.y) / r.dir.y;

    if (tymin > tymax) {
        float temp = tymin;
        tymin = tymax;
        tymax = temp;
    }

    if ((tmin > tymax) || (tymin > tmax))
        return false;

    if (tymin > tmin)
        tmin = tymin;

    if (tymax < tmax)
        tmax = tymax;

    float tzmin = (min.z - r.orig.z) / r.dir.z;
    float tzmax = (max.z - r.orig.z) / r.dir.z;

    if (tzmin > tzmax) {
        float temp = tzmin;
        tzmin = tzmax;
        tzmax = temp;
    }

    if ((tmin > tzmax) || (tzmin > tmax))
        return false;

    if (tzmin > tmin)
        tmin = tzmin;

    if (tzmax < tmax)
        tmax = tzmax;

    *t = tmin;

    return true;
}

vec2 vec2_create(float x, float y) {
    return (vec2){
        .x = x,
        .y = y,
    };
}

vec2 vec2_subtract(vec2 v1, vec2 v2) {
    float x = v1.x - v2.x;
    float y = v1.y - v2.y;
    return vec2_create(x, y);
}

float vec2_length(vec2 v) {
    return sqrtf(v.x * v.x + v.y * v.y);
}

vec2 vec2_normalize(vec2 v) {
    float l = sqrtf(v.x * v.x + v.y * v.y);
    if (l == 0.0f) {
        return v;
    }
    return vec2_create(v.x / l, v.y / l);
}

vec2 vec2_scale(vec2 v, float s) {
    return vec2_create(v.x * s, v.y * s);
}

vec2 vec2_interpolate(vec2 p0, vec2 p1, float a) {
    float x = p0.x + (p1.x - p0.x) * a;
    float y = p0.y + (p1.y - p0.y) * a;
    return vec2_create(x, y);
}

float vec2_distance_squared(vec2 v1, vec2 v2) {
    float dx = v1.x - v2.x;
    float dy = v1.y - v2.y;
    return dx * dx + dy * dy;
}

vec2 vec2_add_scaled(vec2 v1, vec2 v2, float s) {
    float x = v1.x + v2.x * s;
    float y = v1.y + v2.y * s;
    return vec2_create(x, y);
}

vec2 vec2_add(vec2 v1, vec2 v2) {
    float x = v1.x + v2.x;
    float y = v1.y + v2.y;
    return vec2_create(x, y);
}

float vec2_dot(vec2 v1, vec2 v2) {
    return v1.x * v2.x + v1.y * v2.y;
}

float vec2_length_squared(vec2 v) {
    return v.x * v.x + v.y * v.y;
}

vec2 vec2_reflect(vec2 u, vec2 v) {
    return vec2_subtract(vec2_perpindicular_component(u, v), vec2_parallel_component(u, v));
}

vec2 vec2_parallel_component(vec2 u, vec2 v) {
    float l = vec2_dot(u, v) / vec2_length_squared(v);
    return vec2_scale(v, l);
}

vec2 vec2_perpindicular_component(vec2 u, vec2 v) {
    return vec2_subtract(u, vec2_parallel_component(u, v));
}

float vec2_determinant(vec2 u, vec2 v) {
    return u.x * v.y - u.y * v.x;
}

float vec2_distance(vec2 u, vec2 v) {
    float dx = u.x - v.x;
    float dy = u.y - v.y;
    return sqrtf(dx * dx + dy * dy);
}

vec2 vec2_rotate(vec2 v, float theta) {
    vec2 u;
    u.x = v.x * cosf(theta) - v.y * sinf(theta);
    u.y = v.y * cosf(theta) + v.x * sinf(theta);
    return u;
}

vec2 vec2_set_length(vec2 v, float l) {
    return vec2_scale(vec2_normalize(v), l);
}

vec2 vec2_isometric_projection(vec2 v, float scale, float angle) {
    vec2 vp;

    vp = vec2_rotate(v, angle);
    vp.y *= scale;

    return vp;
}

bool vec2_point_left_of_line(vec2 point, vec2 line_p0, vec2 line_p1) {
    vec2 up = vec2_subtract(line_p1, line_p0);
    vec2 left = vec2_rotate(up, 0.5f * M_PI);
    vec2 to_point = vec2_subtract(point, line_p0);
    return vec2_dot(to_point, left) > 0.0f;
}

bool vec2_point_in_polygon(vec2 point, int num_poly_points, vec2 *poly_points) {
    bool is_in = true;

    for (int i = 1; i < num_poly_points; i++) {
        if (!vec2_point_left_of_line(point, poly_points[i - 1], poly_points[i])) {
            is_in = false;
        }
    }
    if (!vec2_point_left_of_line(point, poly_points[num_poly_points - 1], poly_points[0])) {
        is_in = false;
    }

    return is_in;
}

bool vec2_equal(vec2 v1, vec2 v2, float epsilon) {
    return (fabs(v1.x - v2.x) < epsilon) && (fabs(v1.y - v2.y) < epsilon);
}

void vec2_print(vec2 v) {
    printf("<%f, %f>\n", v.x, v.y);
}

vec3 vec3_create(float x, float y, float z) {
    return (vec3){
        .x = x,
        .y = y,
        .z = z};
}

vec3 vec3_add(vec3 v1, vec3 v2) {
    return vec3_create(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

vec3 vec3_add_scaled(vec3 v1, vec3 v2, float s) {
    return vec3_create(
        v1.x + v2.x * s,
        v1.y + v2.y * s,
        v1.z + v2.z * s);
}

vec3 vec3_subtract(vec3 v1, vec3 v2) {
    return vec3_create(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

vec3 vec3_normalize(vec3 v) {
    float l = sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);

    if (l == 0.0) {
        return vec3_create(0.0, 0.0, 0.0);
    }

    return vec3_create(v.x / l, v.y / l, v.z / l);
}

float vec3_dot(vec3 v1, vec3 v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

vec3 vec3_cross(vec3 v1, vec3 v2) {
    float x = v1.y * v2.z - v1.z * v2.y;
    float y = v1.z * v2.x - v1.x * v2.z;
    float z = v1.x * v2.y - v1.y * v2.x;
    return vec3_create(x, y, z);
}

vec3 vec3_apply_mat4(vec3 v, float w, mat4 m) {
    float *a = m.m;
    float x = a[0] * v.x + a[1] * v.y + a[2] * v.z + w * a[3];
    float y = a[4] * v.x + a[5] * v.y + a[6] * v.z + w * a[7];
    float z = a[8] * v.x + a[9] * v.y + a[10] * v.z + w * a[11];
    return vec3_create(x, y, z);
}

vec3 vec3_scale(vec3 v, float s) {
    return vec3_create(s * v.x, s * v.y, s * v.z);
}

float vec3_distance(vec3 v1, vec3 v2) {
    return sqrtf((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y) +
                 (v1.z - v2.z) * (v1.z - v2.z));
}

float vec3_distance_squared(vec3 v1, vec3 v2) {
    return (v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y) +
           (v1.z - v2.z) * (v1.z - v2.z);
}

vec3 vec3_set_length(vec3 v, float l) {
    return vec3_scale(vec3_normalize(v), l);
}

float vec3_length(vec3 v) {
    return sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
}

float vec3_length_squared(vec3 v) {
    return v.x * v.x + v.y * v.y + v.z * v.z;
}

bool vec3_equal(vec3 v1, vec3 v2) {
    return (fabs(v1.x - v2.x) < 0.001) && (fabs(v1.y - v2.y) < 0.001) &&
           (fabs(v1.z - v2.z) < 0.001);
}

vec3 vec3_rotate_y(vec3 v, float theta) {
    float c = cosf(theta);
    float s = sinf(theta);

    float x = c * v.x + s * v.z;
    float y = v.y;
    float z = -s * v.x + c * v.z;

    return vec3_create(x, y, z);
}

/*
 * http://inside.mines.edu/fs_home/gmurray/ArbitraryAxisRotation/
 */
vec3 vec3_rotate_about_axis(vec3 vec, vec3 axis, float theta) {
    float c = cosf(theta);
    float s = sinf(theta);

    float x = vec.x;
    float y = vec.y;
    float z = vec.z;

    float u = axis.x;
    float v = axis.y;
    float w = axis.z;

    vec3 rotated_vec;
    rotated_vec.x = u * (u * x + v * y + w * z) * (1.0 - c) + x * c + (-w * y + v * z) * s;
    rotated_vec.y = v * (u * x + v * y + w * z) * (1.0 - c) + y * c + (w * x - u * z) * s;
    rotated_vec.z = w * (u * x + v * y + w * z) * (1.0 - c) + z * c + (-v * x + u * y) * s;
    return rotated_vec;
}

vec3 vec3_reflect(vec3 u, vec3 v) {
    return vec3_subtract(vec3_perpindicular_component(u, v), vec3_parallel_component(u, v));
}

vec3 vec3_parallel_component(vec3 u, vec3 v) {
    float l = vec3_dot(u, v) / vec3_length_squared(v);
    return vec3_scale(v, l);
}

vec3 vec3_perpindicular_component(vec3 u, vec3 v) {
    return vec3_subtract(u, vec3_parallel_component(u, v));
}

vec3 vec3_interpolate(vec3 v1, vec3 v2, float t) {
    vec3 v;
    v.x = v1.x + (v2.x - v1.x) * t;
    v.y = v1.y + (v2.y - v1.y) * t;
    v.z = v1.z + (v2.z - v1.z) * t;
    return v;
}

vec3 vec3_from_hex_color(int hex_color) {
    vec3 v;
    v.x = 0xFF & (hex_color >> 16);
    v.y = 0xFF & (hex_color >> 8);
    v.z = 0xFF & (hex_color >> 0);
    return vec3_scale(v, 1.0f / 256.0f);
}

void vec3_print(vec3 v) {
    printf("<%f, %f, %f>\n", v.x, v.y, v.z);
}

vec3 vec3_multiply(vec3 v1, vec3 v2) {
    return V3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

vec4 vec4_create(float x, float y, float z, float w) {
    vec4 v;
    v.x = x;
    v.y = y;
    v.z = z;
    v.w = w;
    return v;
}

vec4 vec4_apply_mat(vec4 v, mat4 m) {
    float x = m.m[0] * v.x + m.m[1] * v.y + m.m[2] * v.z + m.m[3] * v.w;
    float y = m.m[4] * v.x + m.m[5] * v.y + m.m[6] * v.z + m.m[7] * v.w;
    float z = m.m[8] * v.x + m.m[9] * v.y + m.m[10] * v.z + m.m[11] * v.w;
    float w = m.m[12] * v.x + m.m[13] * v.y + m.m[14] * v.z + m.m[15] * v.w;
    return (vec4){x, y, z, w};
}

vec4 vec4_scale(vec4 v, float s) {
    return V4(v.x * s, v.y * s, v.z * s, v.w * s);
}

void vec4_normalize_this(vec4 v) {
    float l = sqrt(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w);
    if (l != 0) {
        v.x /= l;
        v.y /= l;
        v.z /= l;
        v.w /= l;
    }
}

void vec4_print(vec4 v) {
    printf("<%f, %f, %f, %f>\n", v.x, v.y, v.z, v.w);
}

mat4 mat4_create(float a, float b, float c, float d,
                 float e, float f, float g, float h,
                 float i, float j, float k, float l,
                 float m, float n, float o, float p) {
    return (mat4){
        .m = {
            a, b, c, d,
            e, f, g, h,
            i, j, k, l,
            m, n, o, p}};
}

mat4 mat4_zero() {
    return mat4_create(
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0);
}

mat4 mat4_identity() {
    return mat4_create(
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_translation(vec3 v) {
    return mat4_create(
        1.0, 0.0, 0.0, v.x,
        0.0, 1.0, 0.0, v.y,
        0.0, 0.0, 1.0, v.z,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_scale(vec3 v) {
    return mat4_create(
        v.x, 0.0, 0.0, 0.0,
        0.0, v.y, 0.0, 0.0,
        0.0, 0.0, v.z, 0.0,
        0.0, 0.0, 0.0, 1.0);
}

/*
 * https://en.wikipedia.org/wiki/Rotation_matrix
 */
mat4 mat4_rotation_x(float theta) {
    float c = cos(theta);
    float s = sin(theta);
    return mat4_create(
        1.0, 0.0, 0.0, 0.0,
        0.0, c, -s, 0.0,
        0.0, s, c, 1.0,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_rotation_y(float theta) {
    float c = cos(theta);
    float s = sin(theta);
    return mat4_create(
        c, 0.0, s, 0.0,
        0.0, 1.0, 0.0, 0.0,
        -s, 0.0, c, 0.0,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_rotation_z(float theta) {
    float c = cos(theta);
    float s = sin(theta);
    return mat4_create(
        c, -s, 0.0, 0.0,
        s, c, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_shear(float theta, vec3 a, vec3 b) {
    float t = tanf(theta);
    return mat4_create(
        a.x * b.x * t + 1.0f, a.x * b.y * t, a.x * b.z * t, 0.0f,
        a.y * b.x * t, a.y * b.y * t + 1.0f, a.y * b.z * t, 0.0f,
        a.z * b.x * t, a.z * b.y * t, a.z * b.z * t + 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f);
}

void mat4_print(mat4 m) {
    float *a = m.m;
    printf("[%f, %f, %f, %f]\n", a[0], a[1], a[2], a[3]);
    printf("[%f, %f, %f, %f]\n", a[4], a[5], a[6], a[7]);
    printf("[%f, %f, %f, %f]\n", a[8], a[9], a[10], a[11]);
    printf("[%f, %f, %f, %f]\n", a[12], a[13], a[14], a[15]);
}

mat4 mat4_multiply_n(int count, ...) {
    va_list args;
    va_start(args, count);
    mat4 result = mat4_identity();
    for (int i = 0; i < count; i++) {
        result = mat4_multiply(result, va_arg(args, mat4));
    }
    va_end(args);
    return result;
}

mat4 mat4_multiply(mat4 m1, mat4 m2) {
    float *a = m1.m;
    float *b = m2.m;

    float c0 = a[0] * b[0] + a[1] * b[4] + a[2] * b[8] + a[3] * b[12];
    float c1 = a[0] * b[1] + a[1] * b[5] + a[2] * b[9] + a[3] * b[13];
    float c2 = a[0] * b[2] + a[1] * b[6] + a[2] * b[10] + a[3] * b[14];
    float c3 = a[0] * b[3] + a[1] * b[7] + a[2] * b[11] + a[3] * b[15];

    float c4 = a[4] * b[0] + a[5] * b[4] + a[6] * b[8] + a[7] * b[12];
    float c5 = a[4] * b[1] + a[5] * b[5] + a[6] * b[9] + a[7] * b[13];
    float c6 = a[4] * b[2] + a[5] * b[6] + a[6] * b[10] + a[7] * b[14];
    float c7 = a[4] * b[3] + a[5] * b[7] + a[6] * b[11] + a[7] * b[15];

    float c8 = a[8] * b[0] + a[9] * b[4] + a[10] * b[8] + a[11] * b[12];
    float c9 = a[8] * b[1] + a[9] * b[5] + a[10] * b[9] + a[11] * b[13];
    float c10 = a[8] * b[2] + a[9] * b[6] + a[10] * b[10] + a[11] * b[14];
    float c11 = a[8] * b[3] + a[9] * b[7] + a[10] * b[11] + a[11] * b[15];

    float c12 = a[12] * b[0] + a[13] * b[4] + a[14] * b[8] + a[15] * b[12];
    float c13 = a[12] * b[1] + a[13] * b[5] + a[14] * b[9] + a[15] * b[13];
    float c14 = a[12] * b[2] + a[13] * b[6] + a[14] * b[10] + a[15] * b[14];
    float c15 = a[12] * b[3] + a[13] * b[7] + a[14] * b[11] + a[15] * b[15];

    return mat4_create(
        c0, c1, c2, c3,
        c4, c5, c6, c7,
        c8, c9, c10, c11,
        c12, c13, c14, c15);
}

mat4 mat4_transpose(mat4 m) {
    float *a = m.m;

    return mat4_create(
        a[0], a[4], a[8], a[12],
        a[1], a[5], a[9], a[13],
        a[2], a[6], a[10], a[14],
        a[3], a[7], a[11], a[15]);
}

mat4 mat4_normal_transform(mat4 m) {
    return mat4_transpose(mat4_inverse(m));
}

/*
 * http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
 */
mat4 mat4_inverse(mat4 m) {
    float *a = m.m;

    float det =
        a[0] * a[5] * a[10] * a[15] +
        a[0] * a[6] * a[11] * a[13] +
        a[0] * a[7] * a[9] * a[14] +

        a[1] * a[4] * a[11] * a[14] +
        a[1] * a[6] * a[8] * a[15] +
        a[1] * a[7] * a[10] * a[12] +

        a[2] * a[4] * a[9] * a[15] +
        a[2] * a[5] * a[11] * a[12] +
        a[2] * a[7] * a[8] * a[13] +

        a[3] * a[4] * a[10] * a[13] +
        a[3] * a[5] * a[8] * a[14] +
        a[3] * a[6] * a[9] * a[12] -

        a[0] * a[5] * a[11] * a[14] -
        a[0] * a[6] * a[9] * a[15] -
        a[0] * a[7] * a[10] * a[13] -

        a[1] * a[4] * a[10] * a[15] -
        a[1] * a[6] * a[11] * a[12] -
        a[1] * a[7] * a[8] * a[14] -

        a[2] * a[4] * a[11] * a[13] -
        a[2] * a[5] * a[8] * a[15] -
        a[2] * a[7] * a[9] * a[12] -

        a[3] * a[4] * a[9] * a[14] -
        a[3] * a[5] * a[10] * a[12] -
        a[3] * a[6] * a[8] * a[13];

    if (det == 0.0) {
        return mat4_identity();
    }

    float a0 = a[5] * a[10] * a[15] + a[6] * a[11] * a[13] + a[7] * a[9] * a[14] - a[5] * a[11] * a[14] - a[6] * a[9] * a[15] - a[7] * a[10] * a[13];

    float a1 = a[1] * a[11] * a[14] + a[2] * a[9] * a[15] + a[3] * a[10] * a[13] - a[1] * a[10] * a[15] - a[2] * a[11] * a[13] - a[3] * a[9] * a[14];

    float a2 = a[1] * a[6] * a[15] + a[2] * a[7] * a[13] + a[3] * a[5] * a[14] - a[1] * a[7] * a[14] - a[2] * a[5] * a[15] - a[3] * a[6] * a[13];

    float a3 = a[1] * a[7] * a[10] + a[2] * a[5] * a[11] + a[3] * a[6] * a[9] - a[1] * a[6] * a[11] - a[2] * a[7] * a[9] - a[3] * a[5] * a[10];

    float a4 = a[4] * a[11] * a[14] + a[6] * a[8] * a[15] + a[7] * a[10] * a[12] - a[4] * a[10] * a[15] - a[6] * a[11] * a[12] - a[7] * a[8] * a[14];

    float a5 = a[0] * a[10] * a[15] + a[2] * a[11] * a[12] + a[3] * a[8] * a[14] - a[0] * a[11] * a[14] - a[2] * a[8] * a[15] - a[3] * a[10] * a[12];

    float a6 = a[0] * a[7] * a[14] + a[2] * a[4] * a[15] + a[3] * a[6] * a[12] - a[0] * a[6] * a[15] - a[2] * a[7] * a[12] - a[3] * a[4] * a[14];

    float a7 = a[0] * a[6] * a[11] + a[2] * a[7] * a[8] + a[3] * a[4] * a[10] - a[0] * a[7] * a[10] - a[2] * a[4] * a[11] - a[3] * a[6] * a[8];

    float a8 = a[4] * a[9] * a[15] + a[5] * a[11] * a[12] + a[7] * a[8] * a[13] - a[4] * a[11] * a[13] - a[5] * a[8] * a[15] - a[7] * a[9] * a[12];

    float a9 = a[0] * a[11] * a[13] + a[1] * a[8] * a[15] + a[3] * a[9] * a[12] - a[0] * a[9] * a[15] - a[1] * a[11] * a[12] - a[3] * a[8] * a[13];

    float a10 = a[0] * a[5] * a[15] + a[1] * a[7] * a[12] + a[3] * a[4] * a[13] - a[0] * a[7] * a[13] - a[1] * a[4] * a[15] - a[3] * a[5] * a[12];

    float a11 = a[0] * a[7] * a[9] + a[1] * a[4] * a[11] + a[3] * a[5] * a[8] - a[0] * a[5] * a[11] - a[1] * a[7] * a[8] - a[3] * a[4] * a[9];

    float a12 = a[4] * a[10] * a[13] + a[5] * a[8] * a[14] + a[6] * a[9] * a[12] - a[4] * a[9] * a[14] - a[5] * a[10] * a[12] - a[6] * a[8] * a[13];

    float a13 = a[0] * a[9] * a[14] + a[1] * a[10] * a[12] + a[2] * a[8] * a[13] - a[0] * a[10] * a[13] - a[1] * a[8] * a[14] - a[2] * a[9] * a[12];

    float a14 = a[0] * a[6] * a[13] + a[1] * a[4] * a[14] + a[2] * a[5] * a[12] - a[0] * a[5] * a[14] - a[1] * a[6] * a[12] - a[2] * a[4] * a[13];

    float a15 = a[0] * a[5] * a[10] + a[1] * a[6] * a[8] + a[2] * a[4] * a[9] - a[0] * a[6] * a[9] - a[1] * a[4] * a[10] - a[2] * a[5] * a[8];

    return mat4_create(
        a0 / det, a1 / det, a2 / det, a3 / det,
        a4 / det, a5 / det, a6 / det, a7 / det,
        a8 / det, a9 / det, a10 / det, a11 / det,
        a12 / det, a13 / det, a14 / det, a15 / det);
}

/*
 * http://www.cs.virginia.edu/~gfx/Courses/1999/intro.fall99.html/lookat.html
 */
mat4 mat4_look_at(vec3 position, vec3 target, vec3 up) {
    vec3 up_p = vec3_normalize(up);
    vec3 f = vec3_normalize(vec3_subtract(target, position));
    vec3 s = vec3_normalize(vec3_cross(f, up_p));
    vec3 u = vec3_normalize(vec3_cross(s, f));

    mat4 M = mat4_create(
        s.x, s.y, s.z, 0.0,
        u.x, u.y, u.z, 0.0,
        -f.x, -f.y, -f.z, 0.0,
        0.0, 0.0, 0.0, 1.0);
    mat4 T = mat4_translation(vec3_scale(position, -1.0));

    return mat4_multiply(M, T);
}

mat4 mat4_from_axes(vec3 x, vec3 y, vec3 z) {
    return mat4_create(
        x.x, x.y, x.z, 0.0f,
        y.x, y.y, y.z, 0.0f,
        z.x, z.y, z.z, 0.0f,
        0.0, 0.0, 0.0, 1.0f);
}

void mat4_get_axes(mat4 m, vec3 *x, vec3 *y, vec3 *z) {
    x->x = m.m[0];
    x->y = m.m[1];
    x->z = m.m[2];

    y->x = m.m[4];
    y->y = m.m[5];
    y->z = m.m[6];

    z->x = m.m[8];
    z->y = m.m[9];
    z->z = m.m[10];
}

/*
 * http://www.cs.virginia.edu/~gfx/Courses/2000/intro.spring00.html/lectures/lecture09/sld017.htm
 */
mat4 mat4_perspective_projection(float fov, float aspect, float near, float far) {
    float f = 1.0 / tan(fov * DEGREES_PER_RADIAN / 2.0);
    float denominator = near - far;

    float a = f / aspect;
    float b = f;
    float c = (far + near) / denominator;
    float d = (2.0 * far * near) / denominator;

    return mat4_create(
        a, 0.0, 0.0, 0.0,
        0.0, b, 0.0, 0.0,
        0.0, 0.0, c, d,
        0.0, 0.0, -1.0, 0.0);
}

/*
 * https://en.wikipedia.org/wiki/Orthographic_projection
 */
mat4 mat4_orthographic_projection(float left, float right, float bottom, float top, float near,
                                  float far) {
    float a = 2.0 / (right - left);
    float b = 2.0 / (top - bottom);
    float c = -2.0 / (far - near);
    float d = -(right + left) / (right - left);
    float e = -(top + bottom) / (top - bottom);
    float f = -(far + near) / (far - near);

    return mat4_create(
        a, 0.0, 0.0, d,
        0.0, b, 0, e,
        0.0, 0.0, c, f,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_from_quat(quat q) {
    float x = q.x;
    float y = q.y;
    float z = q.z;
    float w = q.w;

    return mat4_create(
        1.0 - 2.0 * y * y - 2.0 * z * z, 2.0 * x * y - 2.0 * w * z, 2.0 * x * z + 2.0 * w * y,
        0.0,
        2.0 * x * y + 2.0 * w * z, 1.0 - 2.0 * x * x - 2.0 * z * z, 2.0 * y * z - 2.0 * w * x,
        0.0,
        2.0 * x * z - 2.0 * w * y, 2.0 * y * z + 2.0 * w * x, 1.0 - 2.0 * x * x - 2.0 * y * y,
        0.0,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_box_inertia_tensor(vec3 half_lengths, float mass) {
    return mat4_create(
        (1.0 / 12.0) * mass * (half_lengths.x + half_lengths.y), 0.0, 0.0, 0.0,
        0.0, (1.0 / 12.0) * mass * (half_lengths.y + half_lengths.z), 0.0, 0.0,
        0.0, 0.0, (1.0 / 12.0) * mass * (half_lengths.z + half_lengths.x), 0.0,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_sphere_inertia_tensor(float radius, float mass) {
    return mat4_create(
        (2.0 / 5.0) * mass * radius * radius, 0.0, 0.0, 0.0,
        0.0, (2.0 / 5.0) * mass * radius * radius, 0.0, 0.0,
        0.0, 0.0, (2.0 / 5.0) * mass * radius * radius, 0.0,
        0.0, 0.0, 0.0, 1.0);
}

mat4 mat4_triangle_transform(vec2 p1, vec2 p2, vec2 p3) {
    float a = -1.0f * p2.x + 1.0f * p3.x;
    float b = p1.x - 0.5f * p2.x - 0.5f * p3.x;
    float c = 0.5f * p1.x + 0.25f * p2.x + 0.25f * p3.x;
    float d = -1.0f * p2.y + 1.0f * p3.y;
    float e = p1.y - 0.5f * p2.y - 0.5f * p3.y;
    float f = 0.5f * p1.y + 0.25f * p2.y + 0.25f * p3.y;

    mat4 translation_1 = mat4_translation(vec3_create(0.0f, 0.0f, 1.0f));
    mat4 triangle_mat = mat4_create(a, b, c, 0.0f, d, e, f, 0.0f, 0.0, 0.0f,
                                    1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
    mat4 translation_2 = mat4_translation(vec3_create(0.0f, 0.0f, -1.0f));

    return mat4_multiply(translation_2,
                         mat4_multiply(triangle_mat, translation_1));
}

mat4 mat4_cube_to_line_transform(vec3 p0, vec3 p1, float s) {
    vec3 v0 = vec3_normalize(vec3_subtract(p1, p0));
    vec3 v1 = V3(1.0f, 0.0f, 0.0f);
    vec3 axis = vec3_cross(v0, v1);
    axis = vec3_normalize(axis);
    float angle = -acosf(vec3_dot(v0, v1));
    quat rotation = quat_from_axis_angle(axis, angle);
    mat4 scale_mat = mat4_scale(V3(0.5f * vec3_distance(p0, p1), s, s));
    mat4 rotation_mat = mat4_from_quat(rotation);
    mat4 translation_mat = mat4_translation(vec3_scale(vec3_add(p0, p1), 0.5f));
    return mat4_multiply_n(3, translation_mat, rotation_mat, scale_mat);
}

mat4 mat4_interpolate(mat4 m0, mat4 m1, float t) {
    mat4 m;
    for (int i = 0; i < 16; i++) {
        m.m[i] = m0.m[i] + (m1.m[i] - m0.m[i]) * t;
    }
    return m;
}

quat quat_create(float x, float y, float z, float w) {
    return (quat){
        .x = x,
        .y = y,
        .z = z,
        .w = w};
}

quat quat_create_from_axis_angle(vec3 axis, float angle) {
    float temp = sin(angle / 2.0);
    float x = temp * axis.x;
    float y = temp * axis.y;
    float z = temp * axis.z;
    float w = cos(angle / 2.0);
    return quat_create(x, y, z, w);
}

quat quat_from_axis_angle(vec3 axis, float angle) {
    float temp = sin(angle / 2.0);
    float x = temp * axis.x;
    float y = temp * axis.y;
    float z = temp * axis.z;
    float w = cos(angle / 2.0);
    return quat_create(x, y, z, w);
}

quat quat_multiply(quat u, quat v) {
    float x = v.w * u.x + v.x * u.w - v.y * u.z + v.z * u.y;
    float y = v.w * u.y + v.x * u.z + v.y * u.w - v.z * u.x;
    float z = v.w * u.z - v.x * u.y + v.y * u.x + v.z * u.w;
    float w = v.w * u.w - v.x * u.x - v.y * u.y - v.z * u.z;
    return quat_normalize(quat_create(x, y, z, w));
}

quat quat_normalize(quat q) {
    float mag = sqrt(q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z);

    if (mag == 0) {
        return q;
    }

    return quat_create(q.x / mag, q.y / mag, q.z / mag, q.w / mag);
}

quat quat_interpolate(quat u, quat v, float a) {
    quat r;
    u = quat_normalize(u);
    v = quat_normalize(v);

    r.x = u.x + (v.x - u.x) * a;
    r.y = u.y + (v.y - u.y) * a;
    r.z = u.z + (v.z - u.z) * a;
    r.w = u.w + (v.w - u.w) * a;

    return quat_normalize(r);
}

quat quat_subtract(quat q1, quat q2) {
    return QUAT(q1.x - q2.x, q1.y - q2.y, q1.z - q2.z, q1.w - q2.w);
}

quat quat_add(quat q1, quat q2) {
    return QUAT(q1.x + q2.x, q1.y + q2.y, q1.z + q2.z, q1.w + q2.w);
}

float quat_dot(quat v0, quat v1) {
    return v0.x * v1.x + v0.y * v1.y + v0.z * v1.z + v0.w * v1.w;
}

quat quat_scale(quat v, float s) {
    return QUAT(v.x * s, v.y * s, v.z * s, v.w * s);
}

quat quat_slerp(quat v0, quat v1, float a) {
    v0 = quat_normalize(v0);
    v1 = quat_normalize(v1);

    float dot = quat_dot(v0, v1);
    if (fabs(dot) > 0.95f) {
        return quat_add(v0, quat_scale(quat_subtract(v1, v0), a));
    }

    if (dot < 0.0f) {
        v1 = quat_scale(v1, -1.0f);
        dot = -dot;
    }

    dot = fminf(fmaxf(-1.0f, dot), 1.0f);
    float theta_0 = acosf(dot);
    float theta = theta_0 * a;

    quat v2 = quat_subtract(v1, quat_scale(v0, a));
    v2 = quat_normalize(v2);

    return quat_add(quat_scale(v0, cosf(theta)), quat_scale(v2, sinf(theta)));
}

quat quat_between_vectors(vec3 v1, vec3 v2) {
    quat q;

    vec3 a = vec3_cross(v1, v2);
    q.x = a.x;
    q.y = a.y;
    q.z = a.z;

    float v1_l = vec3_length(v1);
    float v2_l = vec3_length(v2);

    q.w = sqrtf(v1_l * v1_l * v2_l * v2_l) + vec3_dot(v1, v2);

    return quat_normalize(q);
}

void quat_to_axis_angle(quat q, vec3 *v, float *t) {
    if (q.w > 1) {
        q = quat_normalize(q);
    }
    *t = 2.0f * acosf(q.w);
    float s = sqrtf(1.0f - q.w * q.w);
    if (s < 0.001f) {
        v->x = 1.0f;
        v->y = 0.0f;
        v->z = 0.0f;
    } else {
        v->x = q.x / s;
        v->y = q.y / s;
        v->z = q.z / s;
    }
}

void quat_print(quat q) {
    printf("<%f, %f, %f, %f>\n", q.x, q.y, q.z, q.w);
}

