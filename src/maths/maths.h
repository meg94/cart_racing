#ifndef _MATHS_MATHS_H
#define _MATHS_MATHS_H

#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define DEGREES_PER_RADIAN 0.01745329251

float rand_num(void);

typedef struct vec2 {
    float x, y;
} vec2;

typedef struct vec3 {
    float x, y, z;
} vec3;

typedef struct vec4 {
    float x, y, z, w;
} vec4;

typedef struct line_segment_2d {
    vec2 p0, p1;
} line_segment_2d;

/*
 * 4 x 4 matrices
 * Stored in row-order form
 */
typedef struct mat4 {
    float m[16];
} mat4;

typedef struct quat {
    float x, y, z, w;
} quat;

struct ray_2D {
    vec2 orig;
    vec2 dir;
};

struct ray {
    vec3 orig;
    vec3 dir;
};

struct bounding_box {
    vec3 center;
    vec3 half_lengths;
};

struct ray_2D ray_2D_create(vec2 origin, vec2 direction);
vec2 ray_2D_at_time(struct ray_2D r, float t);
bool ray_2D_intersect(struct ray_2D r1, struct ray_2D r2, float *t1, float *t2);

struct ray ray_create(vec3 origin, vec3 direction);
struct bounding_box bounding_box_create(vec3 center, vec3 half_lengths);

bool ray_intersect_bounding_box(struct ray r, struct bounding_box bb, float *t);

#define V2(x, y) \
    (vec2) { x, y }
#define V3(x, y, z) \
    (vec3) { x, y, z }
#define V4(x, y, z, w) \
    (vec4) { x, y, z, w }
#define QUAT quat_create

#define V2_ZERO V2(0.0f, 0.0f)
#define V3_ZERO V3(0.0f, 0.0f, 0.0f)
#define V4_ZERO V4(0.0f, 0.0f, 0.0f, 0.0f)

vec2 vec2_create(float x, float y);
vec2 vec2_subtract(vec2 v1, vec2 v2);
vec2 vec2_normalize(vec2 v);
vec2 vec2_scale(vec2 v, float s);
vec2 vec2_interpolate(vec2 p0, vec2 p1, float a);
float vec2_determinant(vec2 u, vec2 v);
float vec2_length(vec2 v);
float vec2_distance_squared(vec2 v1, vec2 v2);
float vec2_distance(vec2 u, vec2 v);
vec2 vec2_add_scaled(vec2 v1, vec2 v2, float s);
vec2 vec2_add(vec2 v1, vec2 v2);
float vec2_length_squared(vec2 v);
float vec2_dot(vec2 v1, vec2 v2);
vec2 vec2_reflect(vec2 u, vec2 v);
vec2 vec2_parallel_component(vec2 u, vec2 v);
vec2 vec2_rotate(vec2 v, float theta);
vec2 vec2_perpindicular_component(vec2 u, vec2 v);
vec2 vec2_set_length(vec2 v, float l);
vec2 vec2_isometric_projection(vec2 v, float scale, float angle);
bool vec2_point_left_of_line(vec2 point, vec2 line_p0, vec2 line_p1);
bool vec2_point_in_polygon(vec2 point, int num_poly_points, vec2 *poly_points);
bool vec2_equal(vec2 v1, vec2 v2, float epsilon);
void vec2_print(vec2 v);

vec3 vec3_create(float x, float y, float z);
vec3 vec3_add(vec3 v1, vec3 v2);
vec3 vec3_add_scaled(vec3 v1, vec3 v2, float s);
vec3 vec3_subtract(vec3 v1, vec3 v2);
vec3 vec3_normalize(vec3 v);
float vec3_dot(vec3 v1, vec3 v2);
vec3 vec3_cross(vec3 v1, vec3 v2);
vec3 vec3_apply_mat4(vec3 v, float w, mat4 m);
float vec3_distance(vec3 v1, vec3 v2);
float vec3_distance_squared(vec3 v1, vec3 v2);
vec3 vec3_set_length(vec3 v, float l);
float vec3_length(vec3 v);
float vec3_length_squared(vec3 v);
vec3 vec3_scale(vec3 v, float s);
bool vec3_equal(vec3 v1, vec3 v2);
vec3 vec3_rotate_x(vec3 v, float theta);
vec3 vec3_rotate_y(vec3 v, float theta);
vec3 vec3_rotate_z(vec3 v, float theta);
vec3 vec3_rotate_about_axis(vec3 vec, vec3 axis, float theta);
vec3 vec3_reflect(vec3 u, vec3 v);
vec3 vec3_parallel_component(vec3 u, vec3 v);
vec3 vec3_perpindicular_component(vec3 u, vec3 v);
vec3 vec3_interpolate(vec3 v1, vec3 v2, float t);
vec3 vec3_from_hex_color(int hex_color);
vec3 vec3_multiply(vec3 v1, vec3 v2);
void vec3_print(vec3 v);

vec4 vec4_create(float x, float y, float z, float w);
vec4 vec4_apply_mat(vec4 v, mat4 m);
vec4 vec4_scale(vec4 v, float s);
void vec4_normalize_this(vec4 v);
void vec4_print(vec4 v);

mat4 mat4_create(float a, float b, float c, float d,
                 float e, float f, float g, float h,
                 float i, float j, float k, float l,
                 float m, float n, float o, float p);
mat4 mat4_zero();
mat4 mat4_identity();
mat4 mat4_translation(vec3 v);
mat4 mat4_scale(vec3 v);
mat4 mat4_rotation_x(float theta);
mat4 mat4_rotation_y(float theta);
mat4 mat4_rotation_z(float theta);
mat4 mat4_shear(float theta, vec3 a, vec3 b);
mat4 mat4_multiply_n(int count, ...);
mat4 mat4_multiply(mat4 m1, mat4 m2);
mat4 mat4_inverse(mat4 m);
mat4 mat4_transpose(mat4 m);
mat4 mat4_normal_transform(mat4 m);
mat4 mat4_look_at(vec3 position, vec3 target, vec3 up);
mat4 mat4_from_axes(vec3 x, vec3 y, vec3 z);
void mat4_get_axes(mat4 m, vec3 *x, vec3 *y, vec3 *z);
mat4 mat4_perspective_projection(float fov, float aspect, float near, float far);
mat4 mat4_orthographic_projection(float left, float right, float bottom, float top, float near, float far);
mat4 mat4_from_quat(quat q);
mat4 mat4_box_inertia_tensor(vec3 half_lengths, float mass);
mat4 mat4_sphere_inertia_tensor(float radius, float mass);
mat4 mat4_triangle_transform(vec2 p1, vec2 p2, vec2 p3);
mat4 mat4_cube_to_line_transform(vec3 p0, vec3 p1, float s);
mat4 mat4_interpolate(mat4 m0, mat4 m1, float t);
void mat4_print(mat4 m);

quat quat_create(float x, float y, float z, float w);
float quat_dot(quat v0, quat v1);
quat quat_add(quat v0, quat v1);
quat quat_scale(quat v, float s);
quat quat_subtract(quat v0, quat v1);
quat quat_create_from_axis_angle(vec3 axis, float angle);
quat quat_from_axis_angle(vec3 axis, float angle);
quat quat_multiply(quat u, quat v);
quat quat_interpolate(quat u, quat v, float a);
quat quat_slerp(quat u, quat v, float a);
quat quat_normalize(quat q);
quat quat_between_vectors(vec3 v1, vec3 v2);
void quat_to_axis_angle(quat q, vec3 *v, float *t);
void quat_print(quat q);

#endif
