#include "physics.h"

#include "btBulletDynamicsCommon.h"
#include "BulletCollision/Gimpact/btGImpactShape.h"

void init_physics_state(struct physics_state *state) {
    btDefaultCollisionConfiguration *bt_collision_config = new btDefaultCollisionConfiguration();
    btCollisionDispatcher *bt_dispatcher = new btCollisionDispatcher(bt_collision_config);
    btBroadphaseInterface *bt_broadphase_interface = new btDbvtBroadphase();
    btSequentialImpulseConstraintSolver *bt_solver = new btSequentialImpulseConstraintSolver();
    btDiscreteDynamicsWorld *bt_world = new btDiscreteDynamicsWorld(bt_dispatcher, bt_broadphase_interface, bt_solver, bt_collision_config);

    bt_world->setGravity(btVector3(0.0f, -10.0f, 0.0f));

    state->bt_collision_config = bt_collision_config;
    state->bt_dispatcher = bt_dispatcher;
    state->bt_broadphase_interface = bt_broadphase_interface;
    state->bt_solver = bt_solver;
    state->bt_world = bt_world;
}

vec3 local_collider_pos_to_world(struct collider *collider, vec3 local) {
    mat4 transformation = mat4_multiply_n(2,
            mat4_translation(get_collider_position(collider)),
            mat4_from_quat(get_collider_rotation(collider)));
    return vec3_apply_mat4(local, 1.0f, transformation);
}

void update_physics_state(struct game_state *game_state, float dt) {
    struct physics_state *physics = game_state->physics_state;
    btDiscreteDynamicsWorld *bt_world = (btDiscreteDynamicsWorld *) physics->bt_world;
    bt_world->stepSimulation(dt, 0);
}

struct collider_shape *create_box_collider_shape(vec3 half_lengths) {
    btVector3 bt_half_lengths = btVector3(half_lengths.x, half_lengths.y, half_lengths.z);
    btCollisionShape* bt_shape = new btBoxShape(bt_half_lengths);

    struct collider_shape *shape = new struct collider_shape();
    shape->type = COLLIDER_SHAPE_TYPE_BOX;
    shape->bt_shape = bt_shape;
    shape->box.half_lengths = half_lengths;
    return shape;
}

struct collider_shape *create_plane_collider_shape(vec3 normal, float constant) {
    btVector3 bt_normal = btVector3(normal.x, normal.y, normal.z);
    btCollisionShape *bt_shape = new btStaticPlaneShape(bt_normal, constant);

    struct collider_shape *shape = new struct collider_shape();
    shape->type = COLLIDER_SHAPE_TYPE_PLANE;
    shape->bt_shape = bt_shape;
    shape->plane.normal = normal;
    shape->plane.constant = constant;
    return shape;
}

struct collider_shape *create_geometry_collider_shape(struct geometry_array *geometry_array) {
    btTriangleIndexVertexArray *bt_vertex_array = new btTriangleIndexVertexArray();
    for (int i = 0; i < geometry_array->num_geometries; i++) {
        struct geometry *geometry = &geometry_array->geometries[i];

        btIndexedMesh bt_indexed_mesh;

        bt_indexed_mesh.m_numTriangles = geometry->num_faces; 
        bt_indexed_mesh.m_triangleIndexBase = (const unsigned char *) geometry->faces;
        bt_indexed_mesh.m_triangleIndexStride = 3 * sizeof(int);
        bt_indexed_mesh.m_indexType = PHY_INTEGER;

        bt_indexed_mesh.m_numVertices = geometry->num_vertices;
        bt_indexed_mesh.m_vertexBase = (const unsigned char *) geometry->transformed_vertices;
        bt_indexed_mesh.m_vertexStride = 3 * sizeof(float);
        bt_indexed_mesh.m_vertexType = PHY_FLOAT;

        bt_vertex_array->addIndexedMesh(bt_indexed_mesh);
    }

    btCollisionShape *bt_shape;
    bt_shape = new btBvhTriangleMeshShape(bt_vertex_array, true);
    bt_shape->setLocalScaling(btVector3(10.0f, 10.0f, 10.0f));

    struct collider_shape *shape = new struct collider_shape();
    shape->type = COLLIDER_SHAPE_TYPE_MESH;
    shape->bt_shape = bt_shape;
    return shape;
}

struct collider *create_collider(struct physics_state *state, struct collider_shape *shape, struct entity_id entity_id, float mass) {
    btCollisionShape *bt_shape = (btCollisionShape *) shape->bt_shape;
    btDiscreteDynamicsWorld *bt_world = (btDiscreteDynamicsWorld *) state->bt_world;

    struct collider *collider = new struct collider();
    collider->shape = shape;

    btVector3 inertia = btVector3(0.0f, 0.0f, 0.0f);
    if (mass != 0.0f) {
        bt_shape->calculateLocalInertia(mass, inertia);
    }

    btTransform transform;
    transform.setIdentity();
    
    btDefaultMotionState *motion_state = new btDefaultMotionState(transform);
    btRigidBody::btRigidBodyConstructionInfo body_info(mass, motion_state, bt_shape, inertia);

    btRigidBody *bt_body = new btRigidBody(body_info); 
    struct entity_id *user_pointer = new struct entity_id();
    *user_pointer = entity_id;
    bt_body->setUserPointer(user_pointer);
    bt_world->addRigidBody(bt_body);

    collider->bt_body = bt_body;
    return collider;
}

void delete_collider(struct physics_state *state, struct collider *collider) {
    btDiscreteDynamicsWorld *bt_world = (btDiscreteDynamicsWorld *) state->bt_world;
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    struct entity_id *user_pointer = (struct entity_id *) bt_body->getUserPointer();
    btMotionState *bt_motion_state = bt_body->getMotionState();
    bt_world->removeCollisionObject(bt_body);
    delete user_pointer;
    delete bt_motion_state;
    delete bt_body;
    delete collider;
}

struct ray_test_result do_ray_test(struct physics_state *physics, vec3 from, vec3 to) {
    btDiscreteDynamicsWorld *bt_world = (btDiscreteDynamicsWorld *) physics->bt_world;
    struct ray_test_result result;

    btVector3 bt_from(from.x, from.y, from.z);
    btVector3 bt_to(to.x, to.y, to.z);

    btCollisionWorld::ClosestRayResultCallback bt_closest_result(bt_from, bt_to);
    bt_world->rayTest(bt_from, bt_to, bt_closest_result);
    if (bt_closest_result.hasHit()) {
        btVector3 bt_p =  bt_closest_result.m_hitPointWorld;
        btVector3 bt_n =  bt_closest_result.m_hitNormalWorld;

        result.hit = true; 
        result.position = V3(bt_p.getX(), bt_p.getY(), bt_p.getZ());
        result.normal = V3(bt_n.getX(), bt_n.getY(), bt_n.getZ());
        result.t = bt_closest_result.m_closestHitFraction;

        const btCollisionObject *object = bt_closest_result.m_collisionObject;
        struct entity_id *entity_id_ptr = (struct entity_id *) object->getUserPointer();
        if (entity_id_ptr) {
            result.entity_id = *entity_id_ptr;
        } else {
            result.entity_id.idx = 0;
        }
    } else {
        result.hit = false;
    }

    return result;
}

bool ray_test(struct physics_state *physics, vec3 origin, vec3 direction, struct entity_id *id, float *t) {
    btDiscreteDynamicsWorld *bt_world = (btDiscreteDynamicsWorld *) physics->bt_world;

    vec3 from = origin;
    vec3 to = vec3_add(origin, vec3_scale(direction, 100.0f));

    btVector3 bt_from(from.x, from.y, from.z);
    btVector3 bt_to(to.x, to.y, to.z);

    btCollisionWorld::ClosestRayResultCallback bt_closest_result(bt_from, bt_to);
    bt_world->rayTest(bt_from, bt_to, bt_closest_result);

    if (bt_closest_result.hasHit()) {
        const btCollisionObject *object = bt_closest_result.m_collisionObject;
        *t = bt_closest_result.m_closestHitFraction;
        struct entity_id *user_pointer_id = (struct entity_id *) object->getUserPointer();
        if (user_pointer_id) {
            *id = *user_pointer_id;
            return true;
        }
    }

    return false;
}

void apply_force_to_collider(struct collider *collider, vec3 force, vec3 rel_pos) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btVector3 bt_force = btVector3(force.x, force.y, force.z);
    btVector3 bt_rel_pos = btVector3(rel_pos.x, rel_pos.y, rel_pos.z);
    bt_body->applyForce(bt_force, bt_rel_pos);
}

void apply_impulse_to_collider(struct collider *collider, vec3 impulse, vec3 rel_pos) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btVector3 bt_impulse = btVector3(impulse.x, impulse.y, impulse.z);
    btVector3 bt_rel_pos = btVector3(rel_pos.x, rel_pos.y, rel_pos.z);
    bt_body->applyImpulse(bt_impulse, bt_rel_pos);
}

void apply_torque_to_collider(struct collider *collider, vec3 torque) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btVector3 bt_torque = btVector3(torque.x, torque.y, torque.z);
    bt_body->applyTorque(bt_torque);
}

void apply_torque_impulse_to_collider(struct collider *collider, vec3 torque) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btVector3 bt_torque = btVector3(torque.x, torque.y, torque.z);
    bt_body->applyTorqueImpulse(bt_torque);
}

void activate_collider(struct collider *collider) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;
    bt_body->activate(true);
}

vec3 get_collider_position(struct collider *collider) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btTransform bt_world_transform;
    bt_body->getMotionState()->getWorldTransform(bt_world_transform);

    btVector3 bt_p = bt_world_transform.getOrigin();
    return V3(bt_p.getX(), bt_p.getY(), bt_p.getZ());
}

vec3 get_collider_velocity(struct collider *collider) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btVector3 bt_v = bt_body->getLinearVelocity();
    return V3(bt_v.getX(), bt_v.getY(), bt_v.getZ());
}

vec3 get_collider_angular_velocity(struct collider *collider) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btVector3 bt_a = bt_body->getAngularVelocity();
    return V3(bt_a.getX(), bt_a.getY(), bt_a.getZ());
}

quat get_collider_rotation(struct collider *collider) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btTransform bt_world_transform;
    bt_body->getMotionState()->getWorldTransform(bt_world_transform);
    
    btQuaternion bt_q = bt_world_transform.getRotation();
    return QUAT(bt_q.getX(), bt_q.getY(), bt_q.getZ(), bt_q.getW());
}

vec3 get_collider_velocity_at_point(struct collider *collider, vec3 point) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btVector3 bt_com = bt_body->getCenterOfMassPosition();
    btVector3 bt_point = btVector3(point.x, point.y, point.z);
    btVector3 bt_lever = bt_point - bt_com;
    btVector3 bt_velocity = bt_body->getLinearVelocity() + 
        btCross(bt_body->getAngularVelocity(), bt_lever);
    return V3(bt_velocity.getX(), bt_velocity.getY(), bt_velocity.getZ());
}

void set_collider_transform(struct collider *collider, vec3 position, quat rotation) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;

    btTransform bt_transform;
    bt_transform.setIdentity();
    bt_transform.setOrigin(btVector3(position.x, position.y, position.z));
    bt_transform.setRotation(btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));
    bt_body->setWorldTransform(bt_transform);
    bt_body->getMotionState()->setWorldTransform(bt_transform);
}

void set_collider_scale(struct collider *collider, vec3 scale) {
    btCollisionShape* bt_shape = (btCollisionShape *) collider->shape->bt_shape;
    bt_shape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
}

void set_collider_contact_response(struct collider *collider, bool contact_response) {
    btRigidBody *bt_body = (btRigidBody *) collider->bt_body;
    if (contact_response) {
        bt_body->setCollisionFlags(bt_body->getCollisionFlags() | 
                btCollisionObject::CF_NO_CONTACT_RESPONSE);
    } else {
        bt_body->setCollisionFlags(bt_body->getCollisionFlags() & 
                ~btCollisionObject::CF_NO_CONTACT_RESPONSE);
    }
}

bool do_colliders_collide(struct physics_state *physics, 
        struct collider *collider1, struct collider *collider2) {
    btRigidBody *bt_body1 = (btRigidBody *) collider1->bt_body;
    btRigidBody *bt_body2 = (btRigidBody *) collider2->bt_body;
    btDiscreteDynamicsWorld *bt_world = (btDiscreteDynamicsWorld *) physics->bt_world;

    int num_manifolds = bt_world->getDispatcher()->getNumManifolds();
    for (int i = 0; i < num_manifolds; i++) {
        btPersistentManifold* manifold = bt_world->getDispatcher()->getManifoldByIndexInternal(i);
        const btCollisionObject* ob1 = manifold->getBody0();
        const btCollisionObject* ob2 = manifold->getBody1();
        if ((ob1 == bt_body1 && ob2 == bt_body2) ||
                (ob1 == bt_body2 && ob2 == bt_body1)) {
            return true;
        }
    }

    return false;
}

struct collider_shape *get_collider_shape(struct physics_state *physics, const char *name) {
    assert(physics->collider_shapes.count(std::string(name)) > 0);
    return physics->collider_shapes[std::string(name)];
}
