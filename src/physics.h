#ifndef _PHYSICS_H
#define _PHYSICS_H

#include <map>

#include "maths/maths.h"
#include "game.h"
#include "entity.h"

enum collider_shape_type {
    COLLIDER_SHAPE_TYPE_BOX,
    COLLIDER_SHAPE_TYPE_MESH,
    COLLIDER_SHAPE_TYPE_PLANE,
};

struct collider_shape {
    enum collider_shape_type type;
    void *bt_shape;

    union {
        struct {
            vec3 half_lengths;
        } box;

        struct {
            vec3 normal;
            float constant;
        } plane;
    };
};

struct collider {
    void *bt_body;
    struct collider_shape *shape;
};

struct physics_state {
    void *bt_collision_config;
    void *bt_dispatcher;
    void *bt_broadphase_interface;
    void *bt_solver;
    void *bt_world;

    std::map<std::string, struct collider_shape *> collider_shapes;
};

struct ray_test_result {
    bool hit;
    struct entity_id entity_id;
    vec3 position;
    vec3 normal;
    float t;
};

vec3 local_collider_pos_to_world(struct collider *collider, vec3 local);
void init_physics_state(struct physics_state *state);
void update_physics_state(struct game_state *game_state, float dt);
struct collider_shape *create_box_collider_shape(vec3 half_length);
struct collider_shape *create_plane_collider_shape(vec3 normal, float constant);
struct collider_shape *create_geometry_collider_shape(struct geometry_array *geometry_array);
struct collider *create_collider(struct physics_state *state, struct collider_shape *shape, struct entity_id entity_id, float mass);
void delete_collider(struct physics_state *state, struct collider *collider);
bool ray_test(struct physics_state *physics, vec3 origin, vec3 direction, struct entity_id *id, float *t);
struct ray_test_result do_ray_test(struct physics_state *physics, vec3 from, vec3 to);

void apply_force_to_collider(struct collider *collider, vec3 force, vec3 rel_pos);
void apply_impulse_to_collider(struct collider *collider, vec3 impulse, vec3 rel_pos);
void apply_torque_to_collider(struct collider *collider, vec3 torque);
void apply_torque_impulse_to_collider(struct collider *collider, vec3 torque);

void activate_collider(struct collider *collider);
vec3 get_collider_position(struct collider *collider);
vec3 get_collider_velocity(struct collider *collider);
vec3 get_collider_angular_velocity(struct collider *collider);
quat get_collider_rotation(struct collider *collider);
vec3 get_collider_velocity_at_point(struct collider *collider, vec3 point);
void set_collider_transform(struct collider *collider, vec3 position, quat rotation);
void set_collider_scale(struct collider *collider, vec3 scale);
void set_collider_contact_response(struct collider *collider, bool contact_response);
bool do_colliders_collide(struct physics_state *state, 
        struct collider *collider1, struct collider *collider2);

struct collider_shape *get_collider_shape(struct physics_state *physics, const char *name);

#endif
